import client from './client'

export const login = async ({ email, password }) => {
  const { data } = await client.post('/sessions', {
    email,
    password
  })

  return data
}
