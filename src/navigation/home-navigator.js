import { createStackNavigator } from '@react-navigation/stack'
import TabsView from './tabs'
import {
  DeliveryView,
  OrderCompletedView,
  OrderDetailView,
  UpdateUserProfileView
} from '../../screens'
import { useUserContextState } from '../context/user-context'
import {
  HubConnectionBuilder,
  LogLevel,
  HttpTransportType
} from '@microsoft/signalr'
import { BASE_URL } from '../../constants/env'
import { useEffect } from 'react'
import { storage_key } from '../../constants/const'
import AsyncStorage from '@react-native-community/async-storage'

const HomeStack = createStackNavigator()

const HomeStackNavigation = () => {
  const { user, orderId, setOrderId } = useUserContextState()
  useEffect(() => {
    const start = async () => {
      const connection = new HubConnectionBuilder()
        .withUrl(`${BASE_URL}/ws/orderhub`, {
          transport: HttpTransportType.WebSockets
        })
        .configureLogging(LogLevel.Information)
        .withAutomaticReconnect()
        .build()

      await connection.start()
      connection.on('sendOrder', async (userId, orderId) => {
        if (user.id === userId) {
          const data = {
            orderId: orderId,
            userId: userId
          }
          await AsyncStorage.setItem(storage_key.orderId, JSON.stringify(data))
          setOrderId(orderId)
        }
      })
    }
    start()
  }, [])

  return (
    <HomeStack.Navigator
      screenOptions={{
        headerShown: false
      }}>
      {orderId > 0 ? (
        <>
          <HomeStack.Screen name='OrderDetail' component={OrderDetailView} />
          <HomeStack.Screen name='Delivery' component={DeliveryView} />
        </>
      ) : (
        <>
          <HomeStack.Screen name='Home' component={TabsView} />
          <HomeStack.Screen
            name='OrderCompleted'
            component={OrderCompletedView}
          />
          <HomeStack.Screen
            name='UpdateUserProfile'
            component={UpdateUserProfileView}
          />
        </>
      )}
    </HomeStack.Navigator>
  )
}

export default HomeStackNavigation
