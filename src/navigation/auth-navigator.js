import { createStackNavigator } from '@react-navigation/stack'
import {
  GatedView,
  LoginView,
  RegisterView,
  TelegramVerifyView,
  PhoneNumberView,
  VerifyOtpView,
  VerifyEmailView,
  ForgotPasswordView
} from '../../screens'

const AuthStack = createStackNavigator()

const AuthStackNavigation = () => {
  return (
    <AuthStack.Navigator
      screenOptions={{
        headerShown: false
      }}>
      <AuthStack.Screen name='Login' component={LoginView} />
      <AuthStack.Screen name='Register' component={RegisterView} />
      <AuthStack.Screen name='TelegramVerify' component={TelegramVerifyView} />
      <AuthStack.Screen name='VerifyEmail' component={VerifyEmailView} />
      <AuthStack.Screen name='ForgotPassword' component={ForgotPasswordView} />
      {/* <AuthStack.Screen name='PhoneNumber' component={PhoneNumberView} />
            <AuthStack.Screen name='VerifyOtp' component={VerifyOtpView} /> */}
      <AuthStack.Screen name='Gated' component={GatedView} />
    </AuthStack.Navigator>
  )
}

export default AuthStackNavigation
