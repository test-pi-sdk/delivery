import React from 'react'
import { COLORS, icons } from '../../constants'
import { HomeView, UserSettingView } from '../../screens'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import TabBarIconButton from './tab-bar-icon-button'
import CustomTabBar from './custom-tab-bar'
import { Image, StyleSheet } from 'react-native'

const Tab = createBottomTabNavigator()

const TabsView = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        style: { ...styles.container }
      }}
      tabBar={props => <CustomTabBar props={props} />}>
      <Tab.Screen
        name='User'
        component={UserSettingView}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              source={icons.user}
              resizeMode='contain'
              style={{
                ...styles.image,
                tintColor: focused ? COLORS.neonRed : COLORS.white
              }}
            />
          ),
          tabBarButton: props => <TabBarIconButton {...props} />
        }}
      />

      {/* <Tab.Screen
        name='Home'
        component={HomeView}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              source={icons.home}
              resizeMode='contain'
              style={{
                ...styles.image,
                tintColor: focused ? COLORS.neonRed : COLORS.white
              }}
            />
          ),
          tabBarButton: props => <TabBarIconButton {...props} />
        }}
      />

      <Tab.Screen
        name='Search'
        component={HomeView}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              source={icons.search}
              resizeMode='contain'
              style={{
                ...styles.image,
                tintColor: focused ? COLORS.neonRed : COLORS.white
              }}
            />
          ),
          tabBarButton: props => <TabBarIconButton {...props} />
        }}
      />

      <Tab.Screen
        name='LogOut'
        component={HomeView}
        options={{
          tabBarIcon: ({ focused }) => (
            <Image
              source={icons.log_out}
              resizeMode='contain'
              style={{
                ...styles.image,
                tintColor: focused ? COLORS.neonRed : COLORS.white
              }}
            />
          ),
          tabBarButton: props => <TabBarIconButton {...props} />
        }}
      /> */}
    </Tab.Navigator>
  )
}

export default TabsView

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    borderTopWidth: 0,
    backgroundColor: 'transparent',
    elevation: 0
  },
  image: {
    width: 25,
    height: 25
  }
})
