import { Linking, Platform } from 'react-native'

export const openMapApp = address => {
  const scheme = Platform.select({
    ios: 'maps:0,0?q=',
    android: 'geo:0,0?q='
  })
  const label = address
  const url = Platform.select({
    ios: `${scheme}${label}`,
    android: `${scheme}${label}`
  })
  Linking.openURL(url).catch(err => console.log(err))
}
