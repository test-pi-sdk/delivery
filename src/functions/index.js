import { convertDistance } from './convertDistance'
import { callNumber } from './callNumber'
import { openMapApp } from './openMapApp'
import { openTelegram, openZalo } from './openExternalApp'
import { returnApiResponse } from './returnApiResponse'
import { convertSecondToMin } from './converter'
import { checkIfEmail } from './validate'

export {
  convertDistance,
  callNumber,
  openMapApp,
  openTelegram,
  openZalo,
  returnApiResponse,
  convertSecondToMin,
  checkIfEmail
}
