export const convertDistance = (distance = 0) => {
  if (distance >= 1000) {
    return `${distance / 1000}km`
  } else {
    return `${distance}m`
  }
}
