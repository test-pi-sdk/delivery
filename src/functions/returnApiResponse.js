import { responseStatus } from '../../constants/const'

export const returnApiResponse = async (res, auth = true) => {
  if (auth) {
    if (res.status === responseStatus.Unauthorize) {
      return {
        status: res.status,
        data: null
      }
    }
  }
  const result = await res.json()
  return {
    status: res.status,
    data: result
  }
}
