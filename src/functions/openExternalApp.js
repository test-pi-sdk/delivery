import { Linking } from 'react-native'
import { BOT_LINK } from '../../constants/env'

export const openZalo = phone => {
  const url = `https://zalo.me/${phone}`
  Linking.openURL(url).catch(err => console.log(err))
}

export const openTelegram = phone => {
  const url = `https://telegram.me/${phone}`
  Linking.openURL(url).catch(err => console.log(err))
}

export const openTelegramBotLink = uniqueStr => {
  const url = `${BOT_LINK}?start=${uniqueStr}`
  Linking.openURL(url).catch(err => console.log(err))
}
