export const convertSecondToMin = (sec, delay = 0) => {
  return Math.round(sec / 60) + delay
}
