import React from 'react'
import {
  Button,
  Modal,
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import { COLORS, FONTS, SIZES } from '../../constants'

const ModalView = ({ modalVisible, setModalVisible, handleCompleteOrder }) => {
  return (
    <Modal
      visible={modalVisible}
      transparent={true}
      animationType='slide'
      onRequestClose={() => setModalVisible(false)}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <Text
            style={{
              fontSize: SIZES.body2
            }}>
            Xác nhận đã hoàn thành đơn!
          </Text>
          <View style={styles.wrapperBtn}>
            <View
              style={{
                flexDirection: 'row',
                marginTop: SIZES.padding * 2,
                justifyContent: 'space-between'
              }}>
              <TouchableOpacity
                style={{
                  marginRight: 10,
                  ...styles.actionButton
                }}
                onPress={() => setModalVisible(false)}>
                <Text style={{ ...FONTS.h4, color: COLORS.white }}>
                  Quay lại
                </Text>
              </TouchableOpacity>

              <TouchableOpacity
                onPress={handleCompleteOrder}
                style={styles.actionButton}>
                <Text style={{ ...FONTS.h4, color: COLORS.white }}>
                  Đã giao hàng
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  )
}

export default ModalView

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: SIZES.height * 0.2,
    width: SIZES.width * 0.85,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80
  },
  wrapperBtn: {
    width: SIZES.width * 0.75
  },
  actionButton: {
    flex: 1,
    height: 35,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
    borderRadius: 10
  }
})
