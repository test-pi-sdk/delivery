import {
  createContext,
  useState,
  useContext,
  useCallback,
  useEffect
} from 'react'
import AsyncStorage from '@react-native-community/async-storage'
import { Alert, PermissionsAndroid, Platform } from 'react-native'
import Geolocation from '@react-native-community/geolocation'
import { logout, updateCoordinate } from '../services/api/delivery-user'
import {
  responseStatus,
  storage_key,
  tracking_err
} from '../../constants/const'
import { checkOrder } from '../services/api/delivery-order'

const UserContextState = createContext()
const UserContextUpdater = createContext()

const useUserContextState = () => {
  const context = useContext(UserContextState)
  return context
}

const useUserContextUpdater = () => {
  const context = useContext(UserContextUpdater)
  return context
}

const UserContextProvider = ({ children }) => {
  const [user, setUser] = useState(undefined)
  const [location, setLocation] = useState()
  const [orderId, setOrderId] = useState(-1)

  useEffect(() => {
    const interval = setInterval(async () => {
      if (user && location) {
        const res = await updateCoordinate(
          user.id,
          location.latitude,
          location.longitude,
          user.accessToken
        )
        switch (res.status) {
          case responseStatus.Ok:
            getGeoLocation()
            break
          case responseStatus.Unauthorize:
            Alert.alert(
              'Oops!',
              `Tài khoản của bạn đã được đăng nhập ở nơi khác`
            )
            const response = await logout(user.id)
            if (response.status === responseStatus.Ok) {
              AsyncStorage.removeItem(storage_key.user)
              setUser(null)
            } else
              Alert.alert(
                'Oops!',
                `Lỗi Server: ${tracking_err.user_context_logOut}`
              )
            break
          default:
            Alert.alert(
              'Oops!',
              `Lỗi Server: ${tracking_err.user_context_updateCoordinate}`
            )
            break
        }
      }
    }, 1000 * 60 * 5)
    return () => clearInterval(interval)
  }, [])

  const getGeoLocation = () => {
    Geolocation.getCurrentPosition(
      position => setLocation(position.coords),
      error => {
        console.log('The location could not be loaded because', error.message)
        Alert.alert('Warning!', 'Chúng tôi không thể lấy được vị trí của bạn.')
      },
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 }
    )
  }

  const requestLocationPermission = useCallback(async () => {
    try {
      if (Platform.OS === 'ios') {
        Geolocation.requestAuthorization()
        getGeoLocation()
      } else {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message: 'This app requires access to your location',
            buttonNeutral: 'Ask Me Later',
            buttonNegative: 'Cancel',
            buttonPositive: 'OK'
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          getGeoLocation()
        } else {
          Alert.alert('Warning!', 'Chúng tôi cần sử dụng vị trí của bạn.')
        }
      }
    } catch (err) {
      console.warn(err)
    }
  }, [])

  useEffect(() => {
    const getOrderData = async () => {
      const dataJsonString = await AsyncStorage.getItem(storage_key.orderId)
      if (dataJsonString) {
        const storageData = JSON.parse(dataJsonString)
        setOrderId(storageData.orderId)
      } else {
        const res = await checkOrder(user.accessToken, user.id)
        switch (res.status) {
          case responseStatus.Ok:
            if (res.data) {
              const data = {
                orderId: res.data.id,
                userId: user.id
              }
              await AsyncStorage.setItem(
                storage_key.orderId,
                JSON.stringify(data)
              )
              setOrderId(res.data.id)
            }
            break
          case responseStatus.Unauthorize:
            Alert.alert(
              'Oops!',
              `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.check_order}`
            )
            const response = await logout(user.id)
            if (response.status === responseStatus.Ok) {
              AsyncStorage.removeItem(storage_key.user)
              setUser(null)
            } else
              Alert.alert('Oops!', `Lỗi Server: ${tracking_err.check_order}`)
            break
          default:
            Alert.alert(
              'Oops!',
              `Lỗi Server: ${tracking_err.user_context_updateCoordinate}`
            )
            break
        }
      }
    }
    user && getOrderData()
  }, [user])

  useEffect(() => {
    let isMounted = true
    const getUserData = async () => {
      const dataJsonString = await AsyncStorage.getItem(storage_key.user)
      if (dataJsonString) {
        const json = JSON.parse(dataJsonString)
        if (isMounted) setUser(json)
      } else {
        setUser(null)
      }
    }
    requestLocationPermission()
    getUserData()
    return () => {
      isMounted = false
    }
  }, [])

  const logOut = useCallback(async (user, setIsLoading) => {
    setIsLoading(true)
    const res = await logout(user.id)
    if (res.status === responseStatus.Ok) {
      AsyncStorage.removeItem(storage_key.user)
      setUser(null)
    } else Alert.alert('Oops!', `Lỗi Server: ${tracking_err.log_out}`)
    setIsLoading(false)
  }, [])

  const userValue = {
    user,
    setUser,
    location,
    requestLocationPermission,
    orderId,
    setOrderId
  }

  return (
    <UserContextState.Provider value={userValue}>
      <UserContextUpdater.Provider value={logOut}>
        {children}
      </UserContextUpdater.Provider>
    </UserContextState.Provider>
  )
}

export { UserContextProvider, useUserContextState, useUserContextUpdater }
