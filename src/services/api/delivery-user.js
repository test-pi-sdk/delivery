import {
  BASE_URL,
  STRING_TO_HASH,
  CHAT_TELEGRAM_ID
} from '../../../constants/env'
import { tracking_err } from '../../../constants/const'
import { returnApiResponse } from '../../functions/returnApiResponse'
import { makeTelegramBotSendText } from './telegram-bot'
export const HEADERS = { 'Content-Type': 'application/json' }

export const checkEmail = async email => {
  try {
    const payload = {
      email: email
    }
    const res = await fetch(`${BASE_URL}/user/check-email`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    console.error(e)
    const msg = `checkEmail: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const register = async (email, password, chatId) => {
  try {
    const payload = {
      email: email,
      password: `${password}${STRING_TO_HASH}`,
      chatId: chatId
    }
    const res = await fetch(`${BASE_URL}/user/register`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    console.error(e)
    const msg = `register: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const login = async (email, password, latitude, longitude) => {
  try {
    const payload = {
      email: email,
      password: `${password}${STRING_TO_HASH}`,
      latitude: latitude,
      longtitude: longitude
    }
    const res = await fetch(`${BASE_URL}/user/login`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    const msg = `login-${tracking_err.login}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const logout = async id => {
  try {
    const res = await fetch(`${BASE_URL}/user/logout`, {
      method: 'PATCH',
      body: JSON.stringify({ id: id }),
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    console.error(e)
    const msg = `logout: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const getChatIdByEmail = async email => {
  try {
    const res = await fetch(`${BASE_URL}/user/GetChatId?email=${email}`, {
      method: 'GET',
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    console.error(e)
    const msg = `getChatIdByEmail: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const forgotPassword = async (email, password) => {
  try {
    const payload = {
      email: email,
      newPassword: `${password}${STRING_TO_HASH}`
    }
    const res = await fetch(`${BASE_URL}/user/forgot-password`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: HEADERS
    })
    return await returnApiResponse(res, false)
  } catch (e) {
    console.error(e)
    const msg = `forgotPassword: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const updateCoordinate = async (id, lat, lng, token) => {
  try {
    const payload = {
      id: id,
      latitude: lat,
      longtitude: lng
    }
    const res = await fetch(`${BASE_URL}/user/update-coordinate`, {
      method: 'POST',
      body: JSON.stringify(payload),
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res, true)
  } catch (e) {
    console.error(e)
  }
}

export const updateProfile = async (payload, token) => {
  try {
    const res = await fetch(`${BASE_URL}/user/update-profile`, {
      method: 'PATCH',
      body: JSON.stringify(payload),
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res, true)
  } catch (e) {
    console.error(e)
    const msg = `updateProfile: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const getProfile = async (token, id) => {
  try {
    const res = await fetch(`${BASE_URL}/user/get-profile?id=${id}`, {
      method: 'GET',
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res, true)
  } catch (e) {
    console.error(e)
    const msg = `getProfile: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}
