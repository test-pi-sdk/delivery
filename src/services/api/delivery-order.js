import { BASE_URL, CHAT_TELEGRAM_ID } from '../../../constants/env'
import { responseStatus, tracking_err } from '../../../constants/const'
import { returnApiResponse } from '../../functions/returnApiResponse'
import { makeTelegramBotSendText } from './telegram-bot'
export const HEADERS = { 'Content-Type': 'application/json' }

export const getOrderById = async ({ orderId, token }) => {
  try {
    const res = await fetch(
      `${BASE_URL}/order/get-order-info-by-id?id=${orderId}`,
      {
        method: 'GET',
        headers: { ...HEADERS, Authorization: `Bearer ${token}` }
      }
    )
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `getOrderById-${tracking_err.get_order_detail}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
    return {
      status: 500,
      data: null
    }
  }
}

export const receiveOrder = async ({ token, orderId, userId }) => {
  try {
    const body = {
      orderId: orderId,
      shipperId: userId
    }
    const res = await fetch(`${BASE_URL}/order/receive-order`, {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `receiveOrder-${tracking_err.receive_order_detail}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
    return {
      status: 500,
      data: null
    }
  }
}

export const completeOrder = async ({ token, orderId, userId }) => {
  try {
    const body = {
      orderId: orderId,
      shipperId: userId
    }
    const res = await fetch(`${BASE_URL}/order/complete-order`, {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `completeOrder-${tracking_err.complete_order_detail}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
    return {
      status: 500,
      data: null
    }
  }
}

export const rejectOrder = async ({ token, orderId, userId }) => {
  try {
    const body = {
      orderId: orderId,
      shipperId: userId
    }
    const res = await fetch(`${BASE_URL}/order/reject-order`, {
      method: 'PATCH',
      body: JSON.stringify(body),
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `rejectOrder-${tracking_err.reject_order_detail}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
    return {
      status: 500,
      data: null
    }
  }
}

export const receivedOrderByServer = async (token, id) => {
  try {
    const res = await fetch(`${BASE_URL}/order/re-send-order?shipperId=${id}`, {
      method: 'GET',
      headers: { ...HEADERS, Authorization: `Bearer ${token}` }
    })
    if (res?.status === responseStatus.Unauthorize) {
      return {
        status: res.status,
        data: null
      }
    }
    return {
      status: 200
    }
  } catch (e) {
    console.error(e)
    const msg = `receivedOrderByServer-${tracking_err.receive_order_from_server}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const getCompletedOrderByShipperID = async (token, shipperID) => {
  try {
    const res = await fetch(
      `${BASE_URL}/order/get-completed-order?shipperId=${shipperID}`,
      {
        method: 'GET',
        headers: { ...HEADERS, Authorization: `Bearer ${token}` }
      }
    )
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `getCompletedOrderByShipperID-${tracking_err.get_completed_order}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const checkOrder = async (token, shipperID) => {
  try {
    const res = await fetch(
      `${BASE_URL}/order/check-order-of-shipper?shipperId=${shipperID}`,
      {
        method: 'GET',
        headers: { ...HEADERS, Authorization: `Bearer ${token}` }
      }
    )
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `checkOrder-${tracking_err.check_order}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
  }
}

export const getTotal = async distance => {
  try {
    const res = await fetch(
      `${BASE_URL}/order/get-total?distance=${distance}`,
      {
        method: 'GET',
        headers: HEADERS
      }
    )
    return await returnApiResponse(res)
  } catch (e) {
    console.error(e)
    const msg = `getTotal-${tracking_err.get_total}: ${e}`
    makeTelegramBotSendText(msg, CHAT_TELEGRAM_ID)
    return {
      status: 500,
      data: 0
    }
  }
}
