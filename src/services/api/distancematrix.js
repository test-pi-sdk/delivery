import { DISTANCE_MATRIX_API } from '../../../constants/env'

export const getDistance = async ({ origins, destinations }) => {
  try {
    const params = `origins=${origins}&destinations=${destinations}&key=${DISTANCE_MATRIX_API}`
    const res = await fetch(
      `https://api.distancematrix.ai/maps/api/distancematrix/json?${params}`
    )
    const json = await res.json()
    return {
      code: 200,
      data: json
    }
  } catch (error) {
    console.error(error)
    return {
      code: 400,
      data: null
    }
  }
}
