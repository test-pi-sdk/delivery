import { LOCATION_IQ_API } from '../../../constants/env'

export const geocoding = async ({ address: address }) => {
  try {
    const params = `q=${address}&key=${LOCATION_IQ_API}&format=json`
    const res = await fetch(
      `https://us1.locationiq.com/v1/search.php?${params}`
    )
    const json = await res.json()
    return {
      code: 200,
      data: json
    }
  } catch (error) {
    console.error(error)
    return {
      code: 400,
      data: null
    }
  }
}
