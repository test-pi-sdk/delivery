import { BOT_TOKEN } from '../../../constants/env'

const getBotUpdates = () =>
  fetch(`https://api.telegram.org/bot${BOT_TOKEN}/getUpdates`).then(response =>
    response.json()
  )

export const getUserTelegramId = async uniqueString => {
  const { result } = await getBotUpdates()

  const messageUpdates = result.filter(
    ({ message }) => message?.text !== undefined
  )

  const userUpdate = messageUpdates.find(
    ({ message }) => message.text === `/start ${uniqueString}`
  )
  if (userUpdate) return userUpdate.message.from.id
  return null
}

export const makeTelegramBotSendText = (code, chatId) => {
  fetch(`https://api.telegram.org/bot${BOT_TOKEN}/sendMessage`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      text: code,
      chat_id: chatId
    })
  })
}
