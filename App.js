import React from 'react'
import { UserContextProvider } from './src/context/user-context'
import AppRoute from './AppRoute'
import { LogBox } from 'react-native'

LogBox.ignoreLogs(['EventEmitter.removeListener'])
LogBox.ignoreAllLogs()

const App = () => {
  return (
    <UserContextProvider>
      <AppRoute />
    </UserContextProvider>
  )
}

export default App
