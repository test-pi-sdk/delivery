module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: true,
  semi: false,
  singleQuote: true,
  jsxSingleQuote: true,
  trailingComma: 'none',
  bracketSpacing: true,
  tabWidth: 2,
};
