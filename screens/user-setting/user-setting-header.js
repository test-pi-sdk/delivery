import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet } from 'react-native'
import { COLORS, FONTS, icons, SIZES } from '../../constants'
import Loader from '../loader'

const UserSettingHeaderView = ({ isLoading, user, logOut }) => {
  return (
    <View style={styles.container}>
      <View style={{ flex: 1 }}>
        <Text style={{ color: 'black', ...FONTS.h1 }}>Xin chào,</Text>
        <Text style={{ ...FONTS.body2, color: COLORS.black }}>
          {user?.email}
        </Text>
      </View>

      {/* ICON BELL FOR NOTIFY */}
      <View style={{ alignItems: 'center', justifyContent: 'center' }}>
        <TouchableOpacity style={styles.wrapperIcon} onPress={logOut}>
          <Image source={icons.log_out} style={styles.icon} />
          {/* <View style={styles.redDotNotify}></View> */}
        </TouchableOpacity>
      </View>
      {/* END ICON BELL FOR NOTIFY */}
      {isLoading && <Loader loading={isLoading} />}
    </View>
  )
}

export default UserSettingHeaderView

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: SIZES.base * 2,
    marginVertical: SIZES.padding * 2,
    marginHorizontal: SIZES.base * 2,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.white
  },
  wrapperIcon: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: SIZES.largeTitle,
    backgroundColor: COLORS.lightGray3
  },
  icon: {
    width: 25,
    height: 25,
    tintColor: COLORS.primary
  },
  redDotNotify: {
    position: 'absolute',
    top: -5,
    right: -5,
    height: 10,
    width: 10,
    backgroundColor: COLORS.red,
    borderRadius: 5
  }
})
