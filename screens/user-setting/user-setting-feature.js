import React from 'react'
import { View, Text, FlatList } from 'react-native'
import { COLORS, FONTS, SIZES } from '../../constants'
import UserSettingFeatureItem from './user-setting-feature-item'

const UserSettingFeatureView = ({ featuresData }) => {
  const Header = () => (
    <View style={{ marginVertical: SIZES.padding * 2 }}>
      <Text style={{ ...FONTS.h3 }}>Các tính năng</Text>
    </View>
  )
  return (
    <FlatList
      ListHeaderComponent={Header}
      data={featuresData}
      numColumns={4}
      columnWrapperStyle={{ justifyContent: 'space-between' }}
      keyExtractor={item => `${item.id}`}
      renderItem={({ item }) => <UserSettingFeatureItem item={item} />}
      style={{ marginTop: SIZES.padding }}
      contentContainerStyle={{
        paddingHorizontal: SIZES.base * 2,
        marginHorizontal: SIZES.base * 2,
        paddingBottom: 30,
        borderRadius: SIZES.radius,
        backgroundColor: COLORS.white
      }}
    />
  )
}

export default UserSettingFeatureView
