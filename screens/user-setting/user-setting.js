import React from 'react'
import { View } from 'react-native'
import UserSettingHeaderView from './user-setting-header'
import UserSettingFeatureView from './user-setting-feature'
import { COLORS } from '../../constants'
import useUserSettingViewModel from './user-setting.view.model'
import {
  useUserContextState,
  useUserContextUpdater
} from '../../src/context/user-context'

const UserSettingView = () => {
  const { user } = useUserContextState()
  const logOut = useUserContextUpdater()
  const { isLoading, featuresData, handleLogOut } = useUserSettingViewModel(
    user,
    logOut
  )

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: COLORS.lightGray3
      }}>
      <UserSettingHeaderView
        isLoading={isLoading}
        user={user}
        logOut={handleLogOut}
      />
      <UserSettingFeatureView featuresData={featuresData} />
    </View>
  )
}

export default UserSettingView
