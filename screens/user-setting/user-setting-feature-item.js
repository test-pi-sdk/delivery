import {
  Image,
  Text,
  TouchableOpacity,
  View,
  Alert,
  StyleSheet
} from 'react-native'
import { FONTS, SIZES } from '../../constants'
import { useNavigation } from '@react-navigation/native'

const UserSettingFeatureItem = ({ item }) => {
  const navigation = useNavigation()

  return (
    <TouchableOpacity
      style={styles.container}
      onPress={() => {
        if (!item.release) {
          Alert.alert('Oops!', 'Chúng tôi sẽ ra tính năng này sớm!')
        } else {
          navigation.replace(item.link)
        }
      }}>
      <View
        style={{
          backgroundColor: item.backgroundColor,
          ...styles.wrapperIcon
        }}>
        <Image
          source={item.icon}
          resizeMode='contain'
          style={{
            tintColor: item.color,
            ...styles.icon
          }}
        />
      </View>
      <Text
        style={{
          ...styles.description,
          ...FONTS.body4
        }}>
        {item.description}
      </Text>
    </TouchableOpacity>
  )
}

export default UserSettingFeatureItem

const styles = StyleSheet.create({
  container: {
    marginBottom: SIZES.padding * 2,
    width: 80,
    alignItems: 'center'
  },
  wrapperIcon: {
    height: 50,
    width: 50,
    marginBottom: 5,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    width: 25,
    height: 25
  },
  description: {
    textAlign: 'center',
    flexWrap: 'wrap'
  }
})
