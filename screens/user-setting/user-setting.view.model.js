import { COLORS, icons } from '../../constants'
import { useEffect, useCallback, useState } from 'react'
import { responseStatus, tracking_err } from '../../constants/const'
import { Alert } from 'react-native'
import { receivedOrderByServer } from '../../src/services/api/delivery-order'
const useUserSettingViewModel = (user, logOut) => {
  const featuresData = [
    // {
    //   id: 1,
    //   icon: icons.wallet,
    //   color: COLORS.red,
    //   backgroundColor: COLORS.lightRed,
    //   description: 'Ví',
    //   release: false,
    //   link: ''
    // },
    {
      id: 2,
      icon: icons.delivered,
      color: COLORS.yellow,
      backgroundColor: COLORS.lightyellow,
      description: 'Đơn đã giao',
      release: true,
      link: 'OrderCompleted'
    },
    {
      id: 3,
      icon: icons.user,
      color: COLORS.lime,
      backgroundColor: COLORS.lightGreen,
      description: 'Thông tin người dùng',
      release: true,
      link: 'UpdateUserProfile'
    }
    // {
    //   id: 4,
    //   icon: icons.withdraw,
    //   color: COLORS.royalBlue,
    //   backgroundColor: COLORS.lightBlue,
    //   description: 'Rút tiền',
    //   release: false,
    //   link: ''
    // }
  ]
  const [isLoading, setIsLoading] = useState(false)

  const handleLogOut = useCallback(() => {
    logOut(user, setIsLoading)
  }, [])

  useEffect(() => {
    const interval = setInterval(async () => {
      const res = await receivedOrderByServer(user.accessToken, user.id)
      switch (res.status) {
        case responseStatus.Ok:
          break
        case responseStatus.Unauthorize:
          Alert.alert(
            'Oops!',
            `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.receive_order_from_server}`
          )
          logOut(user, setIsLoading)
          break
        default:
          Alert.alert(
            'Oops!',
            `Lỗi Server: ${tracking_err.receive_order_from_server}`
          )
          break
      }
    }, 10000)
    return () => clearInterval(interval)
  }, [])

  return { isLoading, featuresData, handleLogOut }
}

export default useUserSettingViewModel
