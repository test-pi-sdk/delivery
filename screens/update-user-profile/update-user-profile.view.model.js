import { useEffect, useState } from 'react'
import { Alert } from 'react-native'
import { responseStatus, tracking_err } from '../../constants/const'
import { getProfile, updateProfile } from '../../src/services/api/delivery-user'
import { useNavigation } from '@react-navigation/native'

const useUpdateUserProfileViewModel = (user, logOut) => {
  const [displayName, setDisplayName] = useState('')
  const [phone, setPhone] = useState('')
  const [zalo, setZalo] = useState('')
  const [telegram, setTelegram] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const navigation = useNavigation()

  useEffect(() => {
    const fetchData = async () => {
      try {
        setIsLoading(true)
        const res = await getProfile(user.accessToken, user.id)
        switch (res.status) {
          case responseStatus.Ok:
            setDisplayName(res.data.displayName)
            setPhone(res.data.phone)
            setZalo(res.data.zalo)
            setTelegram(res.data.telegram)
            break
          case responseStatus.Unauthorize:
            Alert.alert(
              'Oops!',
              `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.get_user_profile}`
            )
            logOut(user, setIsLoading)
            break
          default:
            Alert.alert('Oops!', `Lỗi Server: ${tracking_err.get_user_profile}`)
            break
        }
      } catch (e) {
        console.error(e)
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.get_user_profile}`)
      } finally {
        setIsLoading(false)
      }
    }
    user && fetchData()
  }, [])

  const handleUpdate = async () => {
    try {
      setIsLoading(true)
      const payload = {
        id: user.id,
        displayName,
        phone,
        zalo,
        telegram
      }
      const res = await updateProfile(payload, user.accessToken)
      switch (res.status) {
        case responseStatus.Ok:
          navigation.replace('Home')
          break
        case responseStatus.Unauthorize:
          Alert.alert(
            'Oops!',
            `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.update_user_profile}`
          )
          logOut(user, setIsLoading)
          break
        default:
          Alert.alert(
            'Oops!',
            `Lỗi Server: ${tracking_err.update_user_profile}`
          )
          break
      }
    } catch (e) {
      console.error(e)
      Alert.alert('Oops!', `Lỗi Server: ${tracking_err.update_user_profile}`)
    } finally {
      setIsLoading(false)
    }
  }

  return {
    displayName,
    phone,
    zalo,
    telegram,
    isLoading,
    setDisplayName,
    setPhone,
    setZalo,
    setTelegram,
    handleUpdate
  }
}

export default useUpdateUserProfileViewModel
