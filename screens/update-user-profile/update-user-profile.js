import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native'
import { COLORS, icons, SIZES } from '../../constants'
import {
  useUserContextState,
  useUserContextUpdater
} from '../../src/context/user-context'
import Loader from '../loader'
import useUpdateUserProfileViewModel from './update-user-profile.view.model'

const UpdateUserProfileView = ({ navigation }) => {
  const { user } = useUserContextState()
  const logOut = useUserContextUpdater()
  const {
    displayName,
    phone,
    zalo,
    telegram,
    isLoading,
    setDisplayName,
    setPhone,
    setZalo,
    setTelegram,
    handleUpdate
  } = useUpdateUserProfileViewModel(user, logOut)

  return (
    <>
      <TouchableOpacity
        style={{
          width: 50,
          paddingLeft: SIZES.padding * 2,
          justifyContent: 'center'
        }}
        onPress={() => navigation.replace('Home')}>
        <Image
          source={icons.back}
          resizeMode='contain'
          style={{
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity>
      <View style={styles.container}>
        <Text style={styles.header}>THÔNG TIN NGƯỜI DÙNG</Text>
        <TextInput
          style={styles.TextInput}
          placeholder='Tên'
          placeholderTextColor='#003f5c'
          value={displayName}
          onChangeText={name => setDisplayName(name)}
        />
        <TextInput
          style={styles.TextInput}
          placeholder='Số điện thoại'
          placeholderTextColor='#003f5c'
          value={phone}
          onChangeText={phone => setPhone(phone)}
        />
        <TextInput
          style={styles.TextInput}
          placeholder='Số đăng ký Zalo'
          placeholderTextColor='#003f5c'
          value={zalo}
          onChangeText={zalo => setZalo(zalo)}
        />
        <TextInput
          style={styles.TextInput}
          placeholder='Số đăng ký Telegram'
          placeholderTextColor='#003f5c'
          value={telegram}
          onChangeText={tel => setTelegram(tel)}
        />
        <TouchableOpacity style={styles.btn} onPress={handleUpdate}>
          <Text style={styles.textBtn}>Cập nhật</Text>
        </TouchableOpacity>
      </View>
      {isLoading && <Loader loading={isLoading} />}
    </>
  )
}

export default UpdateUserProfileView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding2 * 2,
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray3
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30,
    textAlign: 'center'
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: SIZES.padding,
    maxHeight: 50,
    marginBottom: 20,
    alignItems: 'center',
    flex: 1,
    padding: 10
  },
  btn: {
    backgroundColor: COLORS.primary,
    padding: 20,
    borderRadius: SIZES.padding,
    marginBottom: 30
  },
  textBtn: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: COLORS.white
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: SIZES.padding
  }
})
