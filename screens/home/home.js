import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'
import { COLORS } from '../../constants'
import { useUserContextState } from '../../src/context/user-context'
import HeaderView from '../header/header'
import Loader from '../loader'
import OrderListView from '../order-list/order-list'

const HomeView = ({ navigation }) => {
  const { location } = useUserContextState()

  return (
    <>
      {location ? (
        <SafeAreaView style={styles.container}>
          <HeaderView />
          <OrderListView navigation={navigation} location={location} />
        </SafeAreaView>
      ) : (
        <Loader loading={!location} />
      )}
    </>
  )
}

export default HomeView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray4
  }
})
