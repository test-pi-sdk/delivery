import { useState } from 'react'
import { restaurantData } from '../../src/data/fakedata'

const useHomeViewModel = () => {
  const [restaurants, setRestaurants] = useState(restaurantData)
  const onPress = item => {
    console.log(item)
  }

  return {
    restaurants,
    setRestaurants,
    onPress
  }
}

export default useHomeViewModel
