import HomeView from './home/home'
import LoginView from './login/login'
import OrderDetailView from './order-detail/order-detail'
import RenderMapView from './delivery/map-view'
import DeliveryView from './delivery/delivery'
import LoadingView from './loading'
import UserSettingView from './user-setting/user-setting'
import RegisterView from './register/register'
import VerifyOtpView from './otp/otp'
import PhoneNumberView from './otp/phone-number'
import GatedView from './otp/gated'
import TelegramVerifyView from './telegram-verify/telegram-verify'
import ForgotPasswordView from './forgot-password/forgot-password'
import VerifyEmailView from './verify-email.js/verify-email'
import OrderCompletedView from './order-completed/order-completed'
import UpdateUserProfileView from './update-user-profile/update-user-profile'

export {
  HomeView,
  LoginView,
  OrderDetailView,
  RenderMapView,
  DeliveryView,
  LoadingView,
  UserSettingView,
  RegisterView,
  VerifyOtpView,
  PhoneNumberView,
  GatedView,
  TelegramVerifyView,
  ForgotPasswordView,
  VerifyEmailView,
  OrderCompletedView,
  UpdateUserProfileView
}
