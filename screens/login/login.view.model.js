import { useState } from 'react'
import { Alert } from 'react-native'
import { useUserContextState } from '../../src/context/user-context'
import AsyncStorage from '@react-native-community/async-storage'
import { login } from '../../src/services/api/delivery-user'
import {
  responseStatus,
  storage_key,
  tracking_err
} from '../../constants/const'
import { useNavigation } from '@react-navigation/native'

const useLoginViewModel = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const [isLoading, setIsLoading] = useState(false)
  const navigation = useNavigation()

  const { setUser, location } = useUserContextState()
  const onSubmit = async () => {
    let pEmail = email.replace(/\s/g, '')
    let pPass = password.replace(/\s/g, '')
    if (pEmail.length < 5) {
      Alert.alert('Oops!', 'Email phải có ít nhất 5 ký tự.')
      return
    }
    if (pPass.length < 5) {
      Alert.alert('Oops!', 'Mật khẩu phải có ít nhất 5 ký tự.')
      return
    }
    try {
      setIsLoading(true)
      const res = await login(
        pEmail,
        pPass,
        location.latitude,
        location.longitude
      )
      if (res.status === responseStatus.Ok) {
        try {
          await AsyncStorage.setItem(storage_key.user, JSON.stringify(res.data))
          setUser(res.data)
        } catch (error) {
          console.error(e)
          setIsLoading(false)
          Alert.alert('Oops!', `Lỗi Server: ${tracking_err.login_storage}`)
          setUser(null)
        }
      } else {
        setIsLoading(false)
        Alert.alert('Oops!', 'Đăng nhập thất bại.')
      }
    } catch (e) {
      console.error(e)
      Alert.alert('Oops!', `Lỗi Server: ${tracking_err.login}`)
    } finally {
      setIsLoading(false)
    }
  }

  const onPress = () => {
    navigation.navigate('VerifyEmail')
  }

  return {
    email,
    setEmail,
    password,
    setPassword,
    isLoading,
    onSubmit,
    onPress
  }
}

export default useLoginViewModel
