import {
  StyleSheet,
  Image,
  TextInput,
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import useLoginViewModel from './login.view.model'
import { COLORS } from '../../constants'
import Loader from '../loader'

const LoginView = ({ navigation }) => {
  const { setEmail, setPassword, onSubmit, isLoading, onPress } =
    useLoginViewModel()

  return (
    <View style={styles.container}>
      <View style={{ flexDirection: 'row' }}>
        <Image
          source={require('../../assets/icons/pomas.png')}
          style={{ marginRight: 10, ...styles.image }}
        />
        <Image
          source={require('../../assets/icons/pomas_market.png')}
          style={{ marginLeft: 10, ...styles.image }}
        />
      </View>

      <Text style={styles.header}>ĐĂNG NHẬP</Text>
      <TextInput
        style={styles.TextInput}
        placeholder='Email'
        placeholderTextColor='#003f5c'
        onChangeText={email => setEmail(email)}
      />
      <TextInput
        style={styles.TextInput}
        placeholder='Mật khẩu'
        placeholderTextColor='#003f5c'
        secureTextEntry={true}
        onChangeText={password => setPassword(password)}
      />
      <TouchableOpacity style={styles.loginBtn} onPress={onSubmit}>
        <Text style={{ color: COLORS.black }}>Đăng nhập</Text>
      </TouchableOpacity>
      <TouchableOpacity>
        <Text
          style={styles.button}
          onPress={() => navigation.navigate('Register')}>
          Đăng ký
        </Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.button}>Quên mật khẩu?</Text>
      </TouchableOpacity>
      {isLoading && <Loader isLoading={isLoading} />}
    </View>
  )
}

export default LoginView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray3,
    alignItems: 'center',
    justifyContent: 'center'
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50
  },
  header: {
    fontWeight: 'bold',
    marginVertical: 25,
    fontSize: 30,
    color: COLORS.black
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderRadius: 30,
    width: '80%',
    maxHeight: 50,
    borderColor: COLORS.black,
    borderWidth: 1,
    marginBottom: 20,
    alignItems: 'center',
    flex: 1,
    padding: 10
  },
  loginBtn: {
    width: '80%',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    borderColor: COLORS.black,
    borderWidth: 1,
    justifyContent: 'center',
    marginVertical: 20,
    backgroundColor: COLORS.white
  },
  button: {
    height: 30,
    marginTop: 10,
    color: COLORS.black,
    textDecorationLine: 'underline'
  }
})
