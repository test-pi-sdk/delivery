import React, { useState } from 'react'
import { View, TouchableOpacity, Image } from 'react-native'
import { icons, SIZES, COLORS } from '../../constants'
import { useUserContextState } from '../../src/context/user-context'
import Loader from '../loader'

const HeaderView = () => {
  const { user, requestLocationPermission } = useUserContextState()
  const [isLoading, setIsLoading] = useState(false)

  return (
    <View style={{ flexDirection: 'row', height: 50 }}>
      <TouchableOpacity
        style={{
          width: 50,
          paddingLeft: SIZES.padding * 2,
          justifyContent: 'center'
        }}
        onPress={requestLocationPermission}>
        <Image
          source={icons.nearby}
          resizeMode='contain'
          style={{
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity>

      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <View
          style={{
            width: '70%',
            height: '100%',
            backgroundColor: COLORS.lightGray3,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: SIZES.radius
          }}></View>
      </View>

      <TouchableOpacity
        onPress={() => logOut(user, setIsLoading)}
        style={{
          width: 50,
          paddingRight: SIZES.padding * 2,
          justifyContent: 'center'
        }}>
        <Image
          source={icons.log_out}
          resizeMode='contain'
          style={{
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity>
      {isLoading && <Loader loading={isLoading} />}
    </View>
  )
}

export default HeaderView
