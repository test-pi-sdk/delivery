import { useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import {
  checkEmail,
  getChatIdByEmail
} from '../../src/services/api/delivery-user'
import { responseStatus, tracking_err } from '../../constants/const'
import { Alert } from 'react-native'

const useVerifyEmailViewModel = () => {
  const [email, setEmail] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const navigation = useNavigation()

  const handleVerifyEmail = async () => {
    setIsLoading(true)
    const pEmail = email.replace(/\s/g, '')
    const isExistsRes = await checkEmail(pEmail)
    switch (isExistsRes.status) {
      case responseStatus.NotAcceptable:
        const res = await getChatIdByEmail(pEmail)
        if (res.status === responseStatus.Ok)
          navigation.navigate('ForgotPassword', {
            email: pEmail,
            chatId: res.data
          })
        else Alert.alert('Oops!', `Lỗi Server: ${tracking_err.get_chatId}`)
        break
      case responseStatus.Ok:
        Alert.alert('Oops!', 'Email không tồn tại.')
        break
      default:
        Alert.alert(
          'Oops!',
          `Lỗi Server: ${tracking_err.check_email_for_forgot_password}`
        )
        break
    }
    setIsLoading(false)
  }

  return { email, isLoading, setEmail, handleVerifyEmail }
}

export default useVerifyEmailViewModel
