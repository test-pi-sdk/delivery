import React from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { COLORS, SIZES } from '../../constants'
import Loader from '../loader'
import useVerifyEmailViewModel from './verify-email.view.model'

const VerifyEmailView = () => {
  const { setEmail, isLoading, handleVerifyEmail } = useVerifyEmailViewModel()

  return (
    <View style={styles.container}>
      <Text style={styles.header}>QUÊN MẬT KHẨU</Text>
      <TextInput
        style={styles.TextInput}
        placeholder='Nhập email của bạn tại đây'
        placeholderTextColor='#003f5c'
        onChangeText={email => setEmail(email)}
      />
      <TouchableOpacity style={styles.btn} onPress={handleVerifyEmail}>
        <Text style={styles.textBtn}>Xác nhận</Text>
      </TouchableOpacity>

      {isLoading && <Loader loading={isLoading} />}
    </View>
  )
}

export default VerifyEmailView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding2 * 2,
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray3
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30,
    textAlign: 'center'
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: SIZES.padding,
    maxHeight: 50,
    marginBottom: 20,
    alignItems: 'center',
    textAlign: 'center',
    flex: 1,
    padding: 10
  },
  btn: {
    backgroundColor: COLORS.primary,
    padding: 15,
    borderRadius: SIZES.padding,
    marginBottom: 10
  },
  textBtn: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: COLORS.white
  }
})
