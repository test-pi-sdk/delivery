import React from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Image
} from 'react-native'
import { COLORS, icons, SIZES } from '../../constants'
import Loader from '../loader'
import useTelegramVerifyViewModel from './telegram-verify-view.model'

const TelegramVerifyView = ({ route }) => {
  const { onVerify, sendCode, setCode, isLoading, isDisable, onReceiveCode } =
    useTelegramVerifyViewModel(route)
  return (
    <View style={styles.container}>
      <Text style={styles.header}>XÁC THỰC TÀI KHOẢN TELEGRAM</Text>
      <View style={styles.view}>
        <Text>Bước 1: Nhấn vào icon để gửi mã</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <TouchableOpacity style={styles.wrapper} onPress={sendCode}>
          <View style={styles.children}>
            <Image
              source={icons.telegram}
              resizeMode='contain'
              style={styles.image}
            />
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.view}>
        <Text>Bước 2: Nhận mã</Text>
      </View>
      <TouchableOpacity
        disabled={isDisable}
        style={isDisable ? styles.btnDisabled : styles.btn}
        onPress={onReceiveCode}>
        <Text style={styles.textBtn}>Nhận mã</Text>
      </TouchableOpacity>
      <View style={styles.view}>
        <Text>Bước 3: Nhập mã</Text>
      </View>
      <TextInput
        style={styles.TextInput}
        placeholder='Nhập mã bạn nhận được'
        placeholderTextColor='#003f5c'
        onChangeText={code => setCode(code)}
      />
      <View style={styles.view}>
        <Text>Bước 4: Xác thực mã</Text>
      </View>
      <TouchableOpacity
        disabled={!isDisable}
        style={!isDisable ? styles.btnDisabled : styles.btn}
        onPress={onVerify}>
        <Text style={styles.textBtn}>Xác thực</Text>
      </TouchableOpacity>

      {isLoading && <Loader loading={isLoading} />}
    </View>
  )
}

export default TelegramVerifyView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding2 * 2,
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray3
  },
  scrollView: {
    paddingHorizontal: 25
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30,
    textAlign: 'center'
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: SIZES.padding,
    maxHeight: 50,
    marginBottom: 20,
    alignItems: 'center',
    textAlign: 'center',
    flex: 1,
    padding: 10
  },
  btn: {
    backgroundColor: COLORS.primary,
    padding: 15,
    borderRadius: SIZES.padding,
    marginBottom: 10
  },
  btnDisabled: {
    backgroundColor: COLORS.darkgray,
    padding: 15,
    borderRadius: SIZES.padding,
    marginBottom: 10
  },
  textBtn: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: COLORS.white
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: SIZES.padding
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50
  },
  wrapper: {
    marginBottom: SIZES.padding,
    width: 80,
    alignItems: 'center',
    flex: 1
  },
  children: {
    marginBottom: SIZES.padding,
    borderRadius: SIZES.largeTitle,
    padding: SIZES.padding,
    backgroundColor: COLORS.gray
  }
})
