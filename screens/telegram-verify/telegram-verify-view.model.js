import { useNavigation } from '@react-navigation/native'
import { openTelegramBotLink } from '../../src/functions/openExternalApp'
import { useState } from 'react'
import { Alert } from 'react-native'
import {
  getUserTelegramId,
  makeTelegramBotSendText
} from '../../src/services/api/telegram-bot'
import { register } from '../../src/services/api/delivery-user'
import { responseStatus } from '../../constants/const'

const receiveCode = Math.random().toString(36).slice(2)

const useTelegramVerifyViewModel = route => {
  const [code, setCode] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const navigation = useNavigation()
  const unique = Math.random().toString(36).slice(2)
  const [isDisable, setIsDisable] = useState(false)
  const [chatId, setChatId] = useState()

  const onReceiveCode = async () => {
    setIsLoading(true)
    const chat_id = await getUserTelegramId(unique)
    if (chat_id) {
      setIsDisable(!isDisable)
      setChatId(chat_id)
      setIsLoading(false)
      makeTelegramBotSendText(receiveCode, chat_id)
    } else {
      setIsLoading(false)
      Alert.alert('Oops!', 'Vui lòng nhấn Start trong Telegram.')
    }
  }
  const onVerify = async () => {
    setIsLoading(true)
    if (code === receiveCode) {
      const { email, password } = route.params
      const res = await register(email, password, chatId)
      if (res.status === responseStatus.Ok) {
        setIsLoading(false)
        navigation.replace('Gated')
      }
    } else {
      setIsLoading(false)
      Alert.alert('Oops!', 'Mã xác thực không chính xác, vui lòng nhập lại.')
    }
  }

  const sendCode = () => {
    openTelegramBotLink(unique)
  }

  return { onVerify, sendCode, onReceiveCode, setCode, isLoading, isDisable }
}

export default useTelegramVerifyViewModel
