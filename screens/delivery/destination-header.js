import { View, StyleSheet, Image, Text, TouchableOpacity } from 'react-native'
import { COLORS, FONTS, icons, SIZES } from '../../constants'
import { openMapApp } from '../../src/functions'

const DestinationHeaderView = ({ route }) => {
  const { order, total } = route.params
  return (
    <View style={styles.container}>
      <View style={styles.children}>
        <Image
          source={icons.location}
          style={{ ...styles.icon, tintColor: COLORS.red }}
        />
        <View style={{ flex: 1 }}>
          <Text style={{ ...FONTS.body3 }}>{order.distance} km</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Image
            source={icons.bill}
            style={{ ...styles.icon, tintColor: COLORS.green }}
          />
          <Text style={{ ...FONTS.body3 }}>{total} VNĐ</Text>
        </View>
      </View>

      <View style={styles.wrapperBtn}>
        <View
          style={{
            flexDirection: 'row',
            marginTop: SIZES.padding * 2,
            justifyContent: 'space-between'
          }}>
          <TouchableOpacity
            style={{
              marginRight: 10,
              ...styles.actionButton
            }}
            onPress={() => openMapApp(order.deliveryFrom)}>
            <Text style={{ ...FONTS.h4, color: COLORS.white }}>Lấy hàng</Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => openMapApp(order.deliveryTo)}
            style={styles.actionButton}>
            <Text style={{ ...FONTS.h4, color: COLORS.white }}>Giao hàng</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  )
}

export default DestinationHeaderView

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 75,
    left: 0,
    right: 0,
    height: 40,
    alignItems: 'center',
    justifyContent: 'center'
  },
  children: {
    flexDirection: 'row',
    alignItems: 'center',
    width: SIZES.width * 0.9,
    paddingVertical: SIZES.padding,
    paddingHorizontal: SIZES.padding * 2,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.white
  },
  icon: {
    width: 20,
    height: 20,
    marginRight: SIZES.padding
  },
  wrapperBtn: {
    width: SIZES.width * 0.9
  },
  actionButton: {
    flex: 1,
    height: 35,
    marginRight: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary,
    borderRadius: 10
  }
})
