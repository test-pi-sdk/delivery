import { View, StyleSheet, Image } from 'react-native'
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps'
import { COLORS, icons } from '../../constants'

const RenderMapView = ({
  mapView,
  region,
  fromLocation,
  toWarehouse,
  toDestination,
  angle
}) => {
  const destinationMarker = () => {
    return (
      <Marker coordinate={toDestination}>
        <View style={styles.destinationContainer}>
          <View style={styles.destinationChildren}>
            <Image source={icons.pin} style={styles.destinationIcon} />
          </View>
        </View>
      </Marker>
    )
  }

  const warehouseMarker = () => {
    return (
      <Marker coordinate={toWarehouse}>
        <View style={styles.destinationContainer}>
          <View style={styles.destinationChildren}>
            <Image source={icons.warehouse} style={styles.destinationIcon} />
          </View>
        </View>
      </Marker>
    )
  }

  const currentLocationMarkerIcon = () => {
    return (
      <Marker
        coordinate={fromLocation}
        anchor={{ x: 0.5, y: 0.5 }}
        flat={true}
        rotation={angle}>
        <Image source={icons.car} style={styles.currentLocationIcon} />
      </Marker>
    )
  }

  return (
    <View style={{ flex: 1 }}>
      <MapView
        style={{ flex: 1 }}
        ref={mapView}
        provider={PROVIDER_GOOGLE}
        initialRegion={region}>
        {destinationMarker()}
        {warehouseMarker()}
        {currentLocationMarkerIcon()}
      </MapView>
    </View>
  )
}

export default RenderMapView

const styles = StyleSheet.create({
  destinationContainer: {
    height: 40,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.white
  },
  destinationChildren: {
    height: 30,
    width: 30,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: COLORS.primary
  },
  destinationIcon: {
    width: 25,
    height: 25,
    tintColor: COLORS.white
  },
  currentLocationIcon: {
    width: 40,
    height: 40
  }
})
