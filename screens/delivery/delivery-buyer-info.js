import React from 'react'
import { useState } from 'react'
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from 'react-native'
import { COLORS, icons, SIZES, FONTS } from '../../constants'
import { personType } from '../../constants/const'
import ModalView from '../../src/components/modal'
import { callNumber, openTelegram, openZalo } from '../../src/functions'

const DeliveryBuyerInfoView = ({ order, handleCompleteOrder }) => {
  const [modalVisible, setModalVisible] = useState(false)
  const [type, setType] = useState(personType.SELLER)
  const phone = type === personType.BUYER ? order.buyerPhone : order.sellerPhone
  return (
    <>
      <View style={styles.container}>
        <View style={styles.children}>
          <View
            style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <TouchableOpacity
              onPress={() => setType(personType.SELLER)}
              style={
                type === personType.SELLER
                  ? styles.selectedActionButton
                  : styles.actionButton
              }>
              <Text>Lấy hàng</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setType(personType.BUYER)}
              style={
                type === personType.BUYER
                  ? styles.selectedActionButton
                  : styles.actionButton
              }>
              <Text>Giao hàng</Text>
            </TouchableOpacity>
          </View>
          <View style={{ flexDirection: 'row', alignItems: 'center' }}>
            <Image source={icons.user} style={styles.avatar} />
            <View style={{ flex: 1 }}>
              <View
                style={{
                  marginLeft: SIZES.padding,
                  flexDirection: 'row'
                }}>
                <Text
                  style={{
                    color: COLORS.black,
                    flex: 1,
                    fontWeight: 'bold',
                    ...FONTS.body4
                  }}>
                  {type === personType.BUYER
                    ? order.buyerName
                    : order.sellerName}
                </Text>
                <Text
                  style={{
                    color: COLORS.black,
                    marginRight: styles.avatar.width,
                    fontWeight: 'bold',
                    ...FONTS.body4
                  }}>
                  {phone}
                </Text>
              </View>
              <View
                style={{
                  marginHorizontal: SIZES.padding
                }}>
                <Text style={{ color: COLORS.darkgray, ...FONTS.body4 }}>
                  {type === personType.BUYER
                    ? order.deliveryTo
                    : order.deliveryFrom}
                </Text>
                <Text
                  style={{
                    color: COLORS.darkgray,
                    ...FONTS.body4
                  }}>
                  {order.receivedTime}
                </Text>
              </View>
            </View>
          </View>
          {/* Buttons */}
          <View
            style={{
              flexDirection: 'row',
              marginTop: SIZES.padding * 0.5,
              justifyContent: 'space-between'
            }}>
            <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}>
              <TouchableOpacity
                style={styles.containerBtn}
                onPress={() => setModalVisible(true)}>
                <View style={styles.wrapperIcon}>
                  <Image
                    source={icons.check}
                    resizeMode='contain'
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.containerBtn}
                onPress={() => callNumber(phone)}>
                <View style={styles.wrapperIcon}>
                  <Image
                    source={icons.tel}
                    resizeMode='contain'
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.containerBtn}
                onPress={() => openTelegram(phone)}>
                <View style={styles.wrapperIcon}>
                  <Image
                    source={icons.telegram}
                    resizeMode='contain'
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.containerBtn}
                onPress={() => openZalo(phone)}>
                <View style={styles.wrapperIcon}>
                  <Image
                    source={icons.zalo}
                    resizeMode='contain'
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity style={styles.containerBtn} onPress={() => {}}>
                <View style={styles.wrapperIcon}>
                  <Image
                    source={icons.fb}
                    resizeMode='contain'
                    style={styles.icon}
                  />
                </View>
              </TouchableOpacity>
            </ScrollView>
          </View>
        </View>
      </View>
      <ModalView
        modalVisible={modalVisible}
        setModalVisible={setModalVisible}
        handleCompleteOrder={handleCompleteOrder}
      />
    </>
  )
}

export default DeliveryBuyerInfoView

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 25,
    left: 0,
    right: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  children: {
    width: SIZES.width * 0.9,
    paddingBottom: SIZES.padding,
    paddingHorizontal: SIZES.padding,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.white
  },
  avatar: {
    width: 25,
    height: 25,
    borderRadius: 25,
    tintColor: COLORS.pink
  },
  containerBtn: {
    width: 80,
    alignItems: 'center'
  },
  wrapperIcon: {
    height: 35,
    width: 35,
    borderRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center'
  },
  icon: {
    borderRadius: SIZES.radius,
    width: 35,
    height: 35
  },
  selectedActionButton: {
    flex: 1,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.primary
  },
  actionButton: {
    flex: 1,
    height: 35,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: SIZES.radius
  }
})
