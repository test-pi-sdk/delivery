import { useRef, useState, useEffect } from 'react'
import { useNavigation } from '@react-navigation/native'
import { completeOrder } from '../../src/services/api/delivery-order'
import {
  responseStatus,
  storage_key,
  tracking_err
} from '../../constants/const'
import AsyncStorage from '@react-native-community/async-storage'
import { Alert } from 'react-native'

const useDeliveryViewModel = (route, user, setOrderId, logOut) => {
  const { location, order } = route.params
  const navigation = useNavigation()
  const mapView = useRef()
  const [isLoading, setIsLoading] = useState(false)
  const [fromLocation, setFromLocation] = useState({
    latitude: location.latitude,
    longitude: location.longitude
  })
  const [toWarehouse, setToWarehouse] = useState({
    latitude: Number(order.latFrom),
    longitude: Number(order.lngFrom)
  })
  const [toDestination, setToDestination] = useState({
    latitude: Number(order.latTo),
    longitude: Number(order.lngTo)
  })
  const [region, setRegion] = useState(null)

  const [duration, setDuration] = useState(0)
  const [isReady, setIsReady] = useState(false)
  const [angle, setAngle] = useState(0)

  const calculateAngle = coordinates => {
    const startLat = coordinates[0]['latitude']
    const startLng = coordinates[0]['longitude']
    const endLat = coordinates[1]['latitude']
    const endLng = coordinates[1]['longitude']
    let dx = endLat - startLat
    let dy = endLng - startLng

    return (Math.atan2(dy, dx) * 180) / Math.PI
  }

  const zoomIn = () => {
    const newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta / 2,
      longitudeDelta: region.longitudeDelta / 2
    }
    setRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }

  const zoomOut = () => {
    const newRegion = {
      latitude: region.latitude,
      longitude: region.longitude,
      latitudeDelta: region.latitudeDelta * 2,
      longitudeDelta: region.longitudeDelta * 2
    }
    setRegion(newRegion)
    mapView.current.animateToRegion(newRegion, 200)
  }

  useEffect(() => {
    const getPosition = async () => {
      const mapRegion = {
        latitude: (toWarehouse.latitude + toDestination.latitude) / 2,
        longitude: (toWarehouse.longitude + toDestination.longitude) / 2,
        latitudeDelta:
          Math.abs(toWarehouse.latitude - toDestination.latitude) * 2,
        longitudeDelta:
          Math.abs(toWarehouse.longitude - toDestination.longitude) * 2
      }
      setRegion(mapRegion)
    }
    getPosition()
  }, [])

  const handleCompleteOrder = async () => {
    const payload = {
      token: user.accessToken,
      orderId: order.id,
      userId: user.id
    }
    const res = await completeOrder(payload)
    switch (res.status) {
      case responseStatus.Ok:
        await AsyncStorage.removeItem(storage_key.orderId)
        setOrderId(-1)
        navigation.replace('Home')
        break
      case responseStatus.Unauthorize:
        Alert.alert(
          'Oops!',
          `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.complete_order_detail}`
        )
        logOut(user, setIsLoading)
        break
      default:
        Alert.alert(
          'Oops!',
          `Lỗi Server: ${tracking_err.complete_order_detail}`
        )
        break
    }
  }

  return {
    mapView,
    toWarehouse,
    toDestination,
    fromLocation,
    region,
    duration,
    isReady,
    angle,
    isLoading,
    setAngle,
    setFromLocation,
    setToWarehouse,
    setToDestination,
    setIsReady,
    setDuration,
    calculateAngle,
    zoomIn,
    zoomOut,
    handleCompleteOrder
  }
}

export default useDeliveryViewModel
