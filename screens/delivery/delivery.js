import { View } from 'react-native'
import RenderMapView from './map-view'
import DestinationHeaderView from './destination-header'
import DeliveryBuyerInfoView from './delivery-buyer-info'
import useDeliveryViewModel from './delivery.view.model'
import Loader from '../loader'
import {
  useUserContextState,
  useUserContextUpdater
} from '../../src/context/user-context'

const DeliveryView = ({ route, navigation }) => {
  const { user, setOrderId } = useUserContextState()
  const logOut = useUserContextUpdater()
  const {
    mapView,
    region,
    fromLocation,
    toWarehouse,
    toDestination,
    angle,
    isLoading,
    handleCompleteOrder
  } = useDeliveryViewModel(route, user, setOrderId, logOut)
  const { order, total } = route.params

  return (
    <>
      {region ? (
        <View style={{ flex: 1 }}>
          <RenderMapView
            mapView={mapView}
            region={region}
            fromLocation={fromLocation}
            toWarehouse={toWarehouse}
            toDestination={toDestination}
            angle={angle}
          />
          <DestinationHeaderView route={route} />
          <DeliveryBuyerInfoView
            order={order}
            navigation={navigation}
            handleCompleteOrder={handleCompleteOrder}
          />
        </View>
      ) : (
        <Loader loading={!region || isLoading} />
      )}
    </>
  )
}

export default DeliveryView
