import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { SIZES, COLORS, FONTS, icons } from '../../constants'

const OrderDetailBuyerInfo = ({ order }) => {
  return (
    <View style={styles.container}>
      <View style={styles.children}>
        <Text style={{ ...styles.header, ...FONTS.h3 }}>
          THÔNG TIN NGƯỜI MUA
        </Text>
        <View style={{ padding: SIZES.padding * 2 }}>
          <View style={styles.wrap}>
            <Image
              source={icons.user}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>
              Tên người mua: {order.buyerName}
            </Text>
          </View>
          <View style={styles.wrap}>
            <Image
              source={icons.phone}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>SĐT: {order.buyerPhone}</Text>
          </View>
          <View style={styles.wrap}>
            <Image
              source={icons.clock}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>
              Thời gian nhận: {order.receivedTime}
            </Text>
          </View>
          <View style={styles.wrap}>
            <Image
              source={icons.warehouse}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>
              Địa chỉ lấy hàng: {order.deliveryFrom}
            </Text>
          </View>
          <View style={styles.wrap}>
            <Image
              source={icons.location}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>
              Địa chỉ nhận: {order.deliveryTo}
            </Text>
          </View>
          <View style={styles.wrap}>
            <Image
              source={icons.note}
              resizeMode='contain'
              style={styles.iconImage}
            />
            <Text style={styles.marginText}>Ghi chú: {order.note}</Text>
          </View>
        </View>
      </View>
    </View>
  )
}

export default OrderDetailBuyerInfo

const styles = StyleSheet.create({
  container: {
    flex: 6,
    alignItems: 'center',
    paddingVertical: SIZES.padding * 2
  },
  children: {
    width: SIZES.width * 0.9,
    backgroundColor: COLORS.white,
    borderRadius: SIZES.font
  },
  header: { textAlign: 'center', paddingTop: SIZES.padding * 2 },
  wrap: { flexDirection: 'row', paddingBottom: 7 },
  iconImage: { width: 20, height: 20, tintColor: COLORS.primary },
  marginText: {
    marginLeft: 7,
    ...FONTS.body3
  }
})
