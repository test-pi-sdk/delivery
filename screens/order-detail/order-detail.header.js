import React from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import { COLORS, FONTS, icons, SIZES } from '../../constants'

const OrderDetailHeader = ({ navigation }) => {
  return (
    <View style={{ flexDirection: 'row', backgroundColor: COLORS.lightGray2 }}>
      {/* <TouchableOpacity
        style={{
          width: 50,
          paddingLeft: SIZES.padding * 2,
          justifyContent: 'center'
        }}
        onPress={() => navigation.goBack()}>
        <Image
          source={icons.back}
          resizeMode='contain'
          style={{
            width: 30,
            height: 30
          }}
        />
      </c> */}

      <View
        style={{
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center'
        }}>
        <View
          style={{
            height: 50,
            alignItems: 'center',
            justifyContent: 'center',
            paddingHorizontal: SIZES.padding * 3,
            borderRadius: SIZES.radius,
            backgroundColor: COLORS.lightGray2
          }}>
          <Text style={{ ...FONTS.h3 }}></Text>
        </View>
      </View>

      {/* <TouchableOpacity
        style={{
          width: 50,
          paddingRight: SIZES.padding * 2,
          justifyContent: 'center'
        }}>
        <Image
          source={icons.list}
          resizeMode='contain'
          style={{
            width: 30,
            height: 30
          }}
        />
      </TouchableOpacity> */}
    </View>
  )
}

export default OrderDetailHeader
