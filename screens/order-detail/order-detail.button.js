import { COLORS, SIZES, FONTS } from '../../constants'
import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import Loader from '../loader'

const OrderDetailButton = ({
  isLoading,
  handleReceiveOrder,
  handleRejectOrder
}) => {
  return (
    <>
      {!isLoading ? (
        <View style={styles.container}>
          <View style={styles.children}>
            <TouchableOpacity
              style={{
                marginRight: 5,
                ...styles.actionButton
              }}
              onPress={handleRejectOrder}>
              <Text style={{ color: COLORS.white, ...FONTS.h2 }}>Hủy đơn</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                marginLeft: 5,
                ...styles.actionButton
              }}
              onPress={handleReceiveOrder}>
              <Text style={{ color: COLORS.white, ...FONTS.h2 }}>
                Xác nhận đơn
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      ) : (
        <Loader loading={isLoading} />
      )}
    </>
  )
}

export default OrderDetailButton

const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.white,
    borderTopLeftRadius: 40,
    borderTopRightRadius: 40
  },
  children: {
    flexDirection: 'row',
    padding: SIZES.padding * 2,
    alignItems: 'center',
    justifyContent: 'center'
  },
  actionButton: {
    width: SIZES.width * 0.4,
    padding: SIZES.padding,
    backgroundColor: COLORS.primary,
    alignItems: 'center',
    borderRadius: SIZES.radius
  }
})
