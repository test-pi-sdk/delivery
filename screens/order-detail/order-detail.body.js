import React from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import { SIZES, COLORS, FONTS, icons } from '../../constants'
import OrderDetailBuyerInfo from './order-detail.buyer-info'

const OrderDetailBody = ({ order, total }) => {
  return (
    <>
      <OrderDetailBuyerInfo order={order} />
      <View style={styles.container}>
        <View style={styles.children}>
          <View style={{ paddingBottom: SIZES.padding, ...styles.wrapper }}>
            <View style={{ flex: 1, ...styles.wrapper }}>
              <Image
                source={icons.pin}
                resizeMode='contain'
                style={styles.icon}
              />
              <Text style={styles.text}>Khoảng cách</Text>
            </View>
            <Text style={{ textAlign: 'right', ...styles.text }}>
              {order.distance} km
            </Text>
          </View>

          <View style={styles.wrapper}>
            <View style={{ flex: 1, ...styles.wrapper }}>
              <Image
                source={icons.bill}
                resizeMode='contain'
                style={{
                  ...styles.icon,
                  tintColor: COLORS.green
                }}
              />
              <Text style={styles.text}>Tổng tiền</Text>
            </View>
            <Text style={styles.text}>{total} VNĐ</Text>
          </View>
        </View>
      </View>
    </>
  )
}

export default OrderDetailBody

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    paddingVertical: SIZES.padding * 3
  },
  children: {
    justifyContent: 'space-between',
    paddingVertical: SIZES.padding * 2,
    paddingHorizontal: SIZES.padding * 2,
    backgroundColor: COLORS.white,
    borderRadius: SIZES.font,
    width: SIZES.width * 0.9
  },
  wrapper: { flexDirection: 'row' },
  icon: {
    width: 20,
    height: 20,
    tintColor: COLORS.primary
  },
  text: {
    marginLeft: SIZES.padding,
    ...FONTS.h4
  }
})
