import { useEffect, useState } from 'react'
import { Alert } from 'react-native'
import {
  orderStatus,
  responseStatus,
  storage_key,
  tracking_err
} from '../../constants/const'
import {
  getOrderById,
  getTotal,
  receiveOrder,
  rejectOrder
} from '../../src/services/api/delivery-order'
import AsyncStorage from '@react-native-community/async-storage'
import { useNavigation } from '@react-navigation/native'

const useOrderDetailViewModel = (user, location, setOrderId, logOut) => {
  const [order, setOrder] = useState()
  const navigation = useNavigation()
  const [isLoading, setIsLoading] = useState(false)
  const [total, setTotal] = useState(0)

  const handleReceiveOrder = async () => {
    setIsLoading(true)
    const payload = {
      token: user.accessToken,
      orderId: order.id,
      userId: user.id
    }
    const res = await receiveOrder(payload)
    switch (res.status) {
      case responseStatus.Ok:
        navigation.replace('Delivery', {
          location: location,
          order: order,
          total: total
        })
        break
      case responseStatus.Unauthorize:
        Alert.alert(
          'Oops!',
          `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.receive_order_detail}`
        )
        logOut(user, setIsLoading)
        break
      default:
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.receive_order_detail}`)
        break
    }
    setIsLoading(false)
  }

  const handleRejectOrder = async () => {
    setIsLoading(true)
    const payload = {
      token: user.accessToken,
      orderId: order.id,
      userId: user.id
    }
    const res = await rejectOrder(payload)
    switch (res.status) {
      case responseStatus.Ok:
        await AsyncStorage.removeItem(storage_key.orderId)
        setOrder(undefined)
        setOrderId(-1)
        setTotal(0)
        navigation.replace('Home')
        break
      case responseStatus.Unauthorize:
        Alert.alert(
          'Oops!',
          `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.reject_order_detail}`
        )
        logOut(user, setIsLoading)
        break
      default:
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.reject_order_detail}`)
        break
    }
    setIsLoading(false)
  }

  useEffect(() => {
    let isMounted = true
    const fetchData = async () => {
      const dataJsonString = await AsyncStorage.getItem(storage_key.orderId)
      if (dataJsonString) {
        const storageData = JSON.parse(dataJsonString)
        const payload = {
          orderId: storageData.orderId,
          token: user.accessToken
        }
        const res = await getOrderById(payload)
        switch (res.status) {
          case responseStatus.Ok:
            if (isMounted) {
              if (res.data.deliveryUserId == storageData.userId) {
                const resTotal = await getTotal(res.data.distance)
                setTotal(resTotal.data)
                setOrder(res.data)
                if (res.data.status === orderStatus.DELIVERING) {
                  navigation.replace('Delivery', {
                    location: location,
                    order: res.data,
                    total: resTotal.data
                  })
                }
              }
            }
            break
          case responseStatus.Unauthorize:
            Alert.alert(
              'Oops!',
              `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.get_order_detail}`
            )
            logOut(user, setIsLoading)
            break
          default:
            Alert.alert('Oops!', `Lỗi Server: ${tracking_err.get_order_detail}`)
            break
        }
      }
    }
    location && fetchData()
    return () => {
      isMounted = false
    }
  }, [location])

  return { order, total, handleReceiveOrder, handleRejectOrder, isLoading }
}

export default useOrderDetailViewModel
