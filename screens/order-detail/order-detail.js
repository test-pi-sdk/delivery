import React from 'react'
import { StyleSheet, SafeAreaView } from 'react-native'
import { COLORS } from '../../constants'
import {
  useUserContextState,
  useUserContextUpdater
} from '../../src/context/user-context'
import Loader from '../loader'
import OrderDetailBody from './order-detail.body'
import OrderDetailButton from './order-detail.button'
import OrderDetailHeader from './order-detail.header'
import useOrderDetailViewModel from './order-detail.view.model'

const OrderDetailView = ({ navigation }) => {
  const { user, location, setOrderId } = useUserContextState()
  const logOut = useUserContextUpdater()
  const { order, total, handleReceiveOrder, handleRejectOrder, isLoading } =
    useOrderDetailViewModel(user, location, setOrderId, logOut)

  return (
    <>
      {order && location ? (
        <SafeAreaView style={styles.container}>
          <OrderDetailHeader navigation={navigation} />
          <OrderDetailBody order={order} total={total} />
          <OrderDetailButton
            isLoading={isLoading}
            handleRejectOrder={handleRejectOrder}
            handleReceiveOrder={handleReceiveOrder}
          />
        </SafeAreaView>
      ) : (
        <Loader loading={!(order && location)} />
      )}
    </>
  )
}

export default OrderDetailView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray3
  }
})
