import React from 'react'
import { FlatList } from 'react-native'
import { SIZES } from '../../constants'
import useOrderListViewModel from './order-list.view.model'
import Order from './order'

const OrderListView = ({ navigation, location }) => {
  const { restaurants } = useOrderListViewModel()

  return (
    <FlatList
      data={restaurants}
      keyExtractor={item => `${item.id}`}
      renderItem={({ item }) => (
        <Order item={item} location={location} navigation={navigation} />
      )}
      contentContainerStyle={{
        paddingHorizontal: SIZES.padding * 2,
        paddingBottom: 30
      }}
    />
  )
}

export default OrderListView
