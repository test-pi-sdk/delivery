import React from 'react'
import { SafeAreaView, StyleSheet, Text, TouchableOpacity } from 'react-native'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { COLORS, SIZES } from '../../constants'
import useVerifyOtpViewModel from './otp.view.model'

const VerifyOtpView = ({ route, navigation }) => {
  const { phoneNumber } = route.params
  const { invalidCode, setInvalidCode } = useVerifyOtpViewModel()
  return (
    <SafeAreaView style={styles.wrapper}>
      <Text style={styles.prompt}>Nhập mã OTP bạn đã nhận</Text>
      <Text style={styles.message}>
        {`Số điện thoại (${phoneNumber}) của bạn sẽ được sử dụng để bảo mật tài khoản của bạn mỗi khi bạn đăng nhập.`}
      </Text>
      <TouchableOpacity
        style={styles.btnReEnter}
        onPress={() => navigation.replace('PhoneNumber')}>
        <Text>Nhập lại số điện thoại</Text>
      </TouchableOpacity>
      <OTPInputView
        style={{ width: '80%', height: 200 }}
        pinCount={6}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={code => {
          navigation.replace('Gated')
        }}
      />
      {invalidCode && <Text style={styles.error}>Nhập mã sai.</Text>}
    </SafeAreaView>
  )
}

export default VerifyOtpView

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  borderStyleBase: {
    width: 30,
    height: 45
  },

  borderStyleHighLighted: {
    borderColor: '#03DAC6'
  },

  underlineStyleBase: {
    width: 30,
    height: 45,
    borderWidth: 0,
    borderBottomWidth: 1,
    color: 'black',
    fontSize: 20
  },

  underlineStyleHighLighted: {
    borderColor: '#03DAC6'
  },

  prompt: {
    fontSize: 24,
    paddingHorizontal: 30,
    paddingBottom: 20
  },

  message: {
    fontSize: 16,
    paddingHorizontal: 30
  },

  error: {
    color: 'red'
  },

  btnReEnter: {
    borderRadius: SIZES.radius,
    height: 50,
    paddingHorizontal: SIZES.padding * 2,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1
  }
})
