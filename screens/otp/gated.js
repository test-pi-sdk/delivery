import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  TouchableOpacity
} from 'react-native'
import { COLORS, SIZES } from '../../constants'

const GatedView = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.wrapper}>
      <View>
        <Text>Đăng ký thành công!</Text>
      </View>
      <TouchableOpacity
        style={styles.loginBtn}
        onPress={() => navigation.replace('Login')}>
        <Text>Đăng nhập</Text>
      </TouchableOpacity>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  loginBtn: {
    width: '80%',
    borderRadius: SIZES.radius,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    backgroundColor: COLORS.primary
  }
})

export default GatedView
