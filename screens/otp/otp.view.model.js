import { useState, useRef } from 'react'

const useVerifyOtpViewModel = () => {
  const [value, setValue] = useState('')
  const [formattedValue, setFormattedValue] = useState('')
  const phoneInput = useRef(null)
  const [invalidCode, setInvalidCode] = useState(false)
  return {
    value,
    setValue,
    formattedValue,
    setFormattedValue,
    phoneInput,
    invalidCode,
    setInvalidCode
  }
}

export default useVerifyOtpViewModel
