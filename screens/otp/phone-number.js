import React from 'react'
import {
  SafeAreaView,
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native'
import PhoneInput from 'react-native-phone-number-input'
import { COLORS } from '../../constants'
import useVerifyOtpViewModel from './otp.view.model'

const PhoneNumberView = ({ navigation }) => {
  const { value, setValue, formattedValue, setFormattedValue, phoneInput } =
    useVerifyOtpViewModel()

  return (
    <>
      <View style={styles.container}>
        <SafeAreaView style={styles.wrapper}>
          <View style={styles.welcome}>
            <Text>Nhập số điện thoại để nhận mã OTP!</Text>
          </View>
          <PhoneInput
            ref={phoneInput}
            defaultValue={value}
            defaultCode='VN'
            layout='first'
            onChangeText={text => {
              setValue(text)
            }}
            onChangeFormattedText={text => {
              setFormattedValue(text)
            }}
            countryPickerProps={{ withAlphaFilter: true }}
            withShadow
            autoFocus
          />
          <TouchableOpacity
            style={styles.button}
            onPress={() => {
              navigation.navigate('VerifyOtp', {
                phoneNumber: formattedValue
              })
            }}>
            <Text style={styles.buttonText}>Xác nhận</Text>
          </TouchableOpacity>
        </SafeAreaView>
      </View>
    </>
  )
}

export default PhoneNumberView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray3
  },

  wrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  button: {
    marginTop: 20,
    height: 50,
    width: 300,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#7CDB8A',
    shadowColor: 'rgba(0,0,0,0.4)',
    shadowOffset: {
      width: 1,
      height: 5
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10
  },

  buttonText: {
    color: 'white',
    fontSize: 14
  },

  welcome: {
    padding: 20
  },

  status: {
    padding: 20,
    marginBottom: 20,
    justifyContent: 'center',
    alignItems: 'flex-start',
    color: 'gray'
  }
})
