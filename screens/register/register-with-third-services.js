import React from 'react'
import { Alert, Image, StyleSheet, TouchableOpacity, View } from 'react-native'
import { COLORS, icons, SIZES } from '../../constants'

const RegisterWithThirdServicesView = () => {
  const thirdServices = [
    {
      id: 1,
      name: 'google',
      icon: icons.google
    }
  ]

  return (
    <View style={{ flexDirection: 'row' }}>
      {thirdServices.map(thirdService => (
        <TouchableOpacity
          key={`${thirdService.id}-${thirdService.name}`}
          style={styles.container}
          onPress={() => {
            Alert.alert('Oops!', 'Chúng tôi sẽ ra tính năng này sớm!')
          }}>
          <View style={styles.children}>
            <Image
              source={thirdService.icon}
              resizeMode='contain'
              style={styles.image}
            />
          </View>
        </TouchableOpacity>
      ))}
    </View>
  )
}

export default RegisterWithThirdServicesView

const styles = StyleSheet.create({
  container: {
    marginBottom: SIZES.padding,
    width: 80,
    alignItems: 'center',
    flex: 1
  },
  children: {
    marginBottom: SIZES.padding,
    borderRadius: SIZES.largeTitle,
    padding: SIZES.padding,
    backgroundColor: COLORS.gray
  },
  image: {
    height: 25,
    width: 25
  }
})
