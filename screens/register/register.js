import React from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { COLORS, SIZES } from '../../constants'
import RegisterWithThirdServicesView from './register-with-third-services'
import useRegisterViewModel from './register.view.model'

const RegisterView = ({ navigation }) => {
  const { setEmail, setPassword, handleRegister } = useRegisterViewModel()
  return (
    <View style={styles.container}>
      <Text style={styles.header}>ĐĂNG KÝ</Text>
      <TextInput
        style={styles.TextInput}
        placeholder='Email'
        placeholderTextColor='#003f5c'
        onChangeText={email => setEmail(email)}
      />
      <TextInput
        style={styles.TextInput}
        placeholder='Mật khẩu'
        placeholderTextColor='#003f5c'
        secureTextEntry={true}
        onChangeText={password => setPassword(password)}
      />
      {/* <View style={styles.view}>
        <Text>Hoặc đăng nhập bằng</Text>
      </View>
      <RegisterWithThirdServicesView /> */}
      <TouchableOpacity style={styles.btn} onPress={handleRegister}>
        <Text style={styles.textBtn}>Đăng ký</Text>
      </TouchableOpacity>
      <View style={styles.view}>
        <Text>Đã có tài khoản?&nbsp;</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
          <Text style={{ color: COLORS.primary, fontWeight: '700' }}>
            Đăng nhập ngay
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  )
}

export default RegisterView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding2 * 2,
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray3
  },
  scrollView: {
    paddingHorizontal: 25
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30,
    textAlign: 'center'
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: SIZES.padding,
    maxHeight: 50,
    marginBottom: 20,
    alignItems: 'center',
    flex: 1,
    padding: 10
  },
  btn: {
    backgroundColor: COLORS.primary,
    padding: 20,
    borderRadius: SIZES.padding,
    marginBottom: 30
  },
  textBtn: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: COLORS.white
  },
  view: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: SIZES.padding
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 50
  }
})
