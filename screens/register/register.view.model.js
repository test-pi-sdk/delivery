import { useNavigation } from '@react-navigation/native'
import { useState } from 'react'
import { Alert } from 'react-native'
import { responseStatus } from '../../constants/const'
import { checkIfEmail } from '../../src/functions'
import { checkEmail } from '../../src/services/api/delivery-user'

const useRegisterViewModel = () => {
  const [email, setEmail] = useState('')
  const [password, setPassword] = useState('')
  const navigation = useNavigation()
  const handleRegister = async () => {
    let pEmail = email
    let pPass = password
    if (pEmail.replace(/\s/g, '').length < 5) {
      Alert.alert('Oops!', 'Email phải có ít nhất 5 ký tự.')
      return
    }
    if (checkIfEmail(pEmail)) {
      if (pPass.replace(/\s/g, '').length < 5) {
        Alert.alert('Oops!', 'Mật khẩu phải có ít nhất 5 ký tự.')
        return
      }
      try {
        const res = await checkEmail(pEmail)
        if (res.status === responseStatus.Ok)
          navigation.navigate('TelegramVerify', {
            email: pEmail,
            password: pPass
          })
        else Alert.alert('Oops!', 'Email đã tồn tại.')
      } catch (e) {
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.check_email}`)
      }
    } else {
      Alert.alert('Oops!', 'Email không đúng định dạng!')
      return
    }
  }

  return {
    setEmail,
    setPassword,
    handleRegister
  }
}

export default useRegisterViewModel
