import { FlatList, StyleSheet, View } from 'react-native'
import { COLORS, SIZES } from '../../constants'
import {
  useUserContextState,
  useUserContextUpdater
} from '../../src/context/user-context'
import Loader from '../loader'
import OrderCompletedHeader from './order-completed-header'
import useOrderCompletedViewModel from './order-completed.view.model'
import OrderCompletedDetail from './order-completed-detail'

const OrderCompletedView = ({ navigation }) => {
  const { user } = useUserContextState()
  const logOut = useUserContextUpdater()
  const { orders, isLoading } = useOrderCompletedViewModel(user, logOut)
  return (
    <>
      {!isLoading ? (
        <View style={styles.container}>
          <OrderCompletedHeader total={orders.length} navigation={navigation} />
          <FlatList
            data={orders}
            keyExtractor={item => `${item.id}`}
            renderItem={({ item }) => <OrderCompletedDetail item={item} />}
            contentContainerStyle={{
              paddingBottom: 30
            }}
          />
        </View>
      ) : (
        <Loader loading={!isLoading} />
      )}
    </>
  )
}

export default OrderCompletedView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.lightGray3
  }
})
