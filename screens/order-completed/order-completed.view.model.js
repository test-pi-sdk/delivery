import { useState } from 'react'
import { useEffect } from 'react'
import { Alert } from 'react-native'
import { responseStatus, tracking_err } from '../../constants/const'
import { getCompletedOrderByShipperID } from '../../src/services/api/delivery-order'

const useOrderCompletedViewModel = (user, logOut) => {
  const [isLoading, setIsLoading] = useState(false)
  const [orders, setOrders] = useState([])
  useEffect(() => {
    let isMounted = true
    const fetchData = async () => {
      try {
        const res = await getCompletedOrderByShipperID(
          user.accessToken,
          user.id
        )
        switch (res.status) {
          case responseStatus.Ok:
            setOrders(res.data)
            break
          case responseStatus.Unauthorize:
            Alert.alert(
              'Oops!',
              `Tài khoản của bạn đã được đăng nhập ở nơi khác: ${tracking_err.get_completed_order}`
            )
            logOut(user, setIsLoading)
            break
          default:
            Alert.alert(
              'Oops!',
              `Lỗi Server: ${tracking_err.get_completed_order}`
            )
            break
        }
      } catch (e) {
        console.error(e)
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.get_completed_order}`)
      } finally {
        setIsLoading(false)
      }
    }
    user && fetchData()
    return () => {
      isMounted = false
    }
  }, [])
  return { orders, isLoading }
}

export default useOrderCompletedViewModel
