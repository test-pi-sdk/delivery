import React from 'react'
import { View, Text, StyleSheet } from 'react-native'
import { COLORS, FONTS, SIZES } from '../../constants'
import { convertSecondToMin } from '../../src/functions'

const OrderCompletedDetail = ({ item }) => {
  return (
    <View style={styles.container}>
      <View
        style={{
          justifyContent: 'center'
        }}>
        <View style={styles.info}>
          <View style={styles.children}>
            <View style={styles.flex}>
              <Text style={{ ...FONTS.body2 }}>{item.buyerName}</Text>
              <Text style={{ ...FONTS.body2 }}>{item.buyerPhone}</Text>
            </View>
            <View style={styles.flex}>
              <Text
                style={{
                  ...FONTS.body2,
                  textAlign: 'right'
                }}>{`${item.distance} km`}</Text>
              <Text style={{ ...FONTS.body2, textAlign: 'right' }}>
                {item.updatedAt.slice(0, 10)}
              </Text>
            </View>
          </View>
        </View>

        <View style={styles.wrapper}>
          <Text style={{ ...FONTS.h4 }}>
            {`${convertSecondToMin(item.estTime, 0)} - ${convertSecondToMin(
              item.estTime,
              15
            )} phút`}
          </Text>
        </View>
      </View>
    </View>
  )
}

export default OrderCompletedDetail

const styles = StyleSheet.create({
  container: {
    marginVertical: SIZES.padding,
    backgroundColor: COLORS.white,
    padding: SIZES.padding2 * 2
  },
  wrapper: {
    position: 'absolute',
    bottom: 0,
    height: 50,
    width: SIZES.width * 0.3,
    backgroundColor: COLORS.white,
    borderTopRightRadius: SIZES.radius,
    borderBottomLeftRadius: SIZES.radius,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: COLORS.primary,
    borderWidth: 1,
    borderStyle: 'solid',
    shadowColor: COLORS.primary,
    shadowOffset: {
      width: 0,
      height: 7
    },
    shadowOpacity: 0.1,
    shadowRadius: 3,
    elevation: 1
  },
  info: {
    backgroundColor: COLORS.lightGray3,
    height: 150,
    borderTopRightRadius: SIZES.radius,
    borderBottomLeftRadius: SIZES.radius,
    borderColor: COLORS.primary,
    borderWidth: 1,
    borderStyle: 'solid'
  },
  children: {
    flexDirection: 'row',
    paddingBottom: 7
  },
  flex: {
    flex: 1,
    paddingTop: SIZES.padding2 * 1.5,
    paddingHorizontal: SIZES.padding2 * 2
  }
})
