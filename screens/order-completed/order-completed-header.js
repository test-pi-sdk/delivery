import React from 'react'
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native'
import { COLORS, FONTS, icons, SIZES } from '../../constants'

const OrderCompletedHeader = ({ total, navigation }) => {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.children}
        onPress={() => navigation.replace('Home')}>
        <Image source={icons.back} resizeMode='contain' style={styles.image} />
      </TouchableOpacity>
      <View style={styles.wrapper}>
        <View style={styles.text}>
          <Text style={{ ...FONTS.h3 }}>Đã hoàn thành {total} đơn</Text>
        </View>
      </View>
    </View>
  )
}

export default OrderCompletedHeader

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: COLORS.lightGray2
  },
  children: {
    width: 50,
    paddingLeft: SIZES.padding * 2,
    justifyContent: 'center'
  },
  image: {
    width: 30,
    height: 30
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: SIZES.padding * 3,
    borderRadius: SIZES.radius,
    backgroundColor: COLORS.lightGray2
  }
})
