import React from 'react'
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import { COLORS, SIZES } from '../../constants'
import Loader from '../loader'
import useForgotPasswordViewModel from './forgot-password.view.model'

const ForgotPasswordView = ({ route }) => {
  const { setPassword, setCode, isLoading, handleChangePassword } =
    useForgotPasswordViewModel(route)
  return (
    <View style={styles.container}>
      <Text style={styles.header}>QUÊN MẬT KHẨU</Text>
      <TextInput
        style={styles.TextInput}
        placeholder='Nhập mật khẩu mới của bạn tại đây'
        placeholderTextColor='#003f5c'
        onChangeText={password => setPassword(password)}
      />
      <TextInput
        style={styles.TextInput}
        placeholder='Nhập mã bạn nhận từ Telegram tại đây'
        placeholderTextColor='#003f5c'
        onChangeText={code => setCode(code)}
      />
      <TouchableOpacity style={styles.btn} onPress={handleChangePassword}>
        <Text style={styles.textBtn}>Đổi mật khẩu</Text>
      </TouchableOpacity>

      {isLoading && <Loader loading={isLoading} />}
    </View>
  )
}

export default ForgotPasswordView

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: SIZES.padding2 * 2,
    justifyContent: 'center',
    backgroundColor: COLORS.lightGray3
  },
  header: {
    fontWeight: 'bold',
    marginBottom: 25,
    fontSize: 30,
    textAlign: 'center'
  },
  TextInput: {
    backgroundColor: COLORS.white,
    borderColor: COLORS.black,
    borderWidth: 1,
    borderRadius: SIZES.padding,
    maxHeight: 50,
    marginBottom: 20,
    alignItems: 'center',
    textAlign: 'center',
    flex: 1,
    padding: 10
  },
  btn: {
    backgroundColor: COLORS.primary,
    padding: 15,
    borderRadius: SIZES.padding,
    marginBottom: 10
  },
  textBtn: {
    textAlign: 'center',
    fontWeight: '700',
    fontSize: 16,
    color: COLORS.white
  }
})
