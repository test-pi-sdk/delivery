import { useState } from 'react'
import { useNavigation } from '@react-navigation/native'
import { forgotPassword } from '../../src/services/api/delivery-user'
import { responseStatus, tracking_err } from '../../constants/const'
import { Alert } from 'react-native'
import { useEffect } from 'react'
import { makeTelegramBotSendText } from '../../src/services/api/telegram-bot'

const receiveCode = Math.random().toString(36).slice(2)

const useForgotPasswordViewModel = route => {
  const { email, chatId } = route.params
  const [code, setCode] = useState()
  const [password, setPassword] = useState()
  const [isLoading, setIsLoading] = useState(false)
  const navigation = useNavigation()

  useEffect(() => {
    makeTelegramBotSendText(receiveCode, chatId)
  }, [])

  const handleChangePassword = async () => {
    setIsLoading(true)
    const pPassword = password.replace(/\s/g, '')
    if (pPassword.length < 5) {
      Alert.alert('Oops!', 'Mật khẩu phải có ít nhất 5 ký tự.')
      setIsLoading(false)
      return
    }
    if (code === receiveCode) {
      const pEmail = email.replace(/\s/g, '')
      const res = await forgotPassword(pEmail, pPassword)
      if (res.status === responseStatus.Ok) {
        setIsLoading(false)
        Alert.alert('Đổi mật khẩu thành công.', '', [
          {
            text: 'Đăng nhập',
            onPress: () => navigation.replace('Login'),
            style: 'login'
          }
        ])
      } else {
        setIsLoading(false)
        Alert.alert('Oops!', `Lỗi Server: ${tracking_err.forgot_password}`)
      }
    } else {
      setIsLoading(false)
      Alert.alert('Oops!', 'Mã xác thực không chính xác, vui lòng nhập lại.')
    }
  }

  return { isLoading, setPassword, setCode, handleChangePassword }
}

export default useForgotPasswordViewModel
