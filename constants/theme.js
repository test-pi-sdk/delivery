import { Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')

export const COLORS = {
  // base colors
  // primary: '#FC6D3F', // orange
  primary: '#63B7EE',
  secondary: '#CDCDD2', // gray

  // colors
  black: '#1E1F20',
  white: '#FFFFFF',
  red: '#FF0000',
  neonRed: '#FF3131',
  redOrange: '#FF4433',

  lightGray: '#F5F5F6',
  lightGray2: '#F6F6F7',
  lightGray3: '#EFEFF1',
  lightGray4: '#F8F8F9',
  transparent: 'transparent',
  darkgray: '#898C95',

  green: '#66D59A',
  lightGreen: '#E6FEF0',

  lime: '#00BA63',
  emerald: '#2BC978',

  lightRed: '#FFF1F0',

  purple: '#6B3CE9',
  lightpurple: '#F3EFFF',

  yellow: '#FFC664',
  lightyellow: '#FFF9EC',

  gray: '#C1C3C5',

  //blue
  aqua: '#00FFFF',
  lightBlue: '#CCFFFF',
  neonBlue: '#1F51FF',
  skyBlue: '#87CEEB',
  royalBlue: '#4169E1',
  turquoise: '#40E0D0',

  pink: '#ffc0cb'
}

export const SIZES = {
  // global sizes
  base: 8,
  font: 14,
  radius: 30,
  padding: 10,
  padding2: 12,

  // font sizes
  largeTitle: 50,
  h1: 30,
  h2: 22,
  h3: 20,
  h4: 18,
  body1: 30,
  body2: 20,
  body3: 16,
  body4: 14,
  body5: 12,

  // app dimensions
  width,
  height
}

export const FONTS = {
  largeTitle: {
    fontSize: SIZES.largeTitle,
    lineHeight: 55
  },
  h1: { fontSize: SIZES.h1, lineHeight: 36 },
  h2: { fontSize: SIZES.h2, lineHeight: 30 },
  h3: { fontSize: SIZES.h3, lineHeight: 22 },
  h4: { fontSize: SIZES.h4, lineHeight: 22 },
  body1: {
    fontSize: SIZES.body1,
    lineHeight: 36
  },
  body2: {
    fontSize: SIZES.body2,
    lineHeight: 30
  },
  body3: {
    fontSize: SIZES.body3,
    lineHeight: 22
  },
  body4: {
    fontSize: SIZES.body4,
    lineHeight: 22
  },
  body5: { fontSize: SIZES.body5, lineHeight: 22 }
}

const appTheme = { COLORS, SIZES, FONTS }

export default appTheme
