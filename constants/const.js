export const pricePerMeters = 4

export const responseStatus = {
  Ok: 200,
  BadRequest: 400,
  Unauthorize: 401,
  NotFound: 404,
  NotAcceptable: 406,
  ServerError: 500
}

export const storage_key = {
  user: 'USER_DATA',
  orderId: 'ORDER_DATA'
}

export const orderStatus = {
  PENDING: 0,
  DELIVERING: 1,
  COMPLETED: 2,
  REJECTED: 3
}

export const personType = {
  SELLER: 'seller',
  BUYER: 'buyer'
}

export const tracking_err = {
  user_context_updateCoordinate: 10000000,
  user_context_logOut: 10000001,
  receive_order_detail: 2844950046,
  reject_order_detail: 5018965257,
  get_order_detail: 2112987379,
  complete_order_detail: 7835533762,
  log_out: 6159679966,
  check_email: 6159674774,
  check_email_for_forgot_password: 8608542360,
  get_chatId: 6955637786,
  forgot_password: 3491289234,
  login: 9071447942,
  login_storage: 2614991317,
  receive_order_from_server: 1417510048,
  get_completed_order: 1145177004,
  check_order: 3865403762,
  get_total: 4893503762,
  get_user_profile: 2804563762,
  update_user_profile: 2839763762,
}
