export const back = require('../assets/icons/back.png')
export const car = require('../assets/icons/car.png')
export const cutlery = require('../assets/icons/cutlery.png')
export const donut = require('../assets/icons/donut.png')
export const drink = require('../assets/icons/drink.png')
export const fire = require('../assets/icons/fire.png')
export const fries = require('../assets/icons/fries.png')
export const hamburger = require('../assets/icons/hamburger.png')
export const hotdog = require('../assets/icons/hotdog.png')
export const like = require('../assets/icons/like.png')
export const list = require('../assets/icons/list.png')
export const location = require('../assets/icons/location.png')
export const master_card = require('../assets/icons/mastercard.png')
export const nearby = require('../assets/icons/nearby.png')
export const noodle = require('../assets/icons/noodle.png')
export const pin = require('../assets/icons/pin.png')
export const pizza = require('../assets/icons/pizza.png')
export const red_pin = require('../assets/icons/red-pin.png')
export const rice_bowl = require('../assets/icons/rice-bowl.png')
export const salad = require('../assets/icons/salad.png')
export const search = require('../assets/icons/search.png')
export const basket = require('../assets/icons/shopping-basket.png')
export const star = require('../assets/icons/star.png')
export const sushi = require('../assets/icons/sushi.png')
export const user = require('../assets/icons/user.png')
export const phone = require('../assets/icons/phone.png')
export const clock = require('../assets/icons/clock.png')
export const note = require('../assets/icons/note.png')
export const helicopter = require('../assets/icons/helicopter.png')
export const log_out = require('../assets/icons/logout.png')

export const bell = require('../assets/icons/bell.png')
export const bill = require('../assets/icons/bill.png')
export const close = require('../assets/icons/close.png')
export const disable_eye = require('../assets/icons/disable_eye.png')
export const down = require('../assets/icons/down.png')
export const eye = require('../assets/icons/eye.png')
export const game = require('../assets/icons/games.png')
export const barcode = require('../assets/icons/barcode.png')
export const info = require('../assets/icons/info.png')
export const internet = require('../assets/icons/internet.png')
export const more = require('../assets/icons/more.png')
export const mobile_phone = require('../assets/icons/mobile_phone.png')
export const reload = require('../assets/icons/reload.png')
export const scan = require('../assets/icons/scan.png')
export const send = require('../assets/icons/send.png')
export const wallet = require('../assets/icons/wallet.png')
export const delivered = require('../assets/icons/delivered.png')
export const withdraw = require('../assets/icons/withdraw.png')
export const google = require('../assets/icons/google.png')
export const pomas = require('../assets/icons/pomas.png')
export const pomas_market = require('../assets/icons/pomas_market.png')
export const home = require('../assets/icons/home.png')
export const warehouse = require('../assets/icons/warehouse.png')
export const tel = require('../assets/icons/tel.png')
export const telegram = require('../assets/icons/telegram.png')
export const zalo = require('../assets/icons/zalo.png')
export const check = require('../assets/icons/check.png')
export const fb = require('../assets/icons/fb.png')

export default {
  back,
  bell,
  bill,
  home,
  close,
  disable_eye,
  down,
  eye,
  game,
  barcode,
  info,
  internet,
  more,
  mobile_phone,
  reload,
  car,
  scan,
  send,
  wallet,
  cutlery,
  donut,
  drink,
  fire,
  fries,
  hamburger,
  hotdog,
  like,
  list,
  location,
  master_card,
  nearby,
  noodle,
  pin,
  pizza,
  red_pin,
  rice_bowl,
  salad,
  search,
  basket,
  star,
  sushi,
  user,
  phone,
  clock,
  note,
  helicopter,
  log_out,
  delivered,
  withdraw,
  google,
  pomas,
  pomas_market,
  warehouse,
  tel,
  telegram,
  zalo,
  check,
  fb
}
