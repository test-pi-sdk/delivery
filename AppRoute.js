import React, { useEffect } from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import { LoadingView } from './screens'
import { useUserContextState } from './src/context/user-context'
import { HomeStackNavigation, AuthStackNavigation } from './src/navigation'

const Stack = createStackNavigator()

const AppRoute = () => {
  const { user, location } = useUserContextState()

  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          headerShown: false
        }}>
        {user ? (
          <Stack.Screen name='Home' component={HomeStackNavigation} />
        ) : user === undefined || !location ? (
          <Stack.Screen name='Loading' component={LoadingView} />
        ) : (
          user === null && (
            <Stack.Screen name='Auth' component={AuthStackNavigation} />
          )
        )}
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default AppRoute
